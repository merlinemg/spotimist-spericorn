from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404
from .forms import SupportRequestForm, SupportRequestReplyForm
from .models import SupportRequest

# Create your views here.

def contact_form (request):

    if request.method != "POST":
        if request.user.is_authenticated():

            data = {'name' : request.user.get_full_name() , 'email': request.user.email}
            form = SupportRequestForm(initial=data)
        else:
            form = SupportRequestForm()

    else:
        form = SupportRequestForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            if request.user.is_authenticated():
                instance.user = request.user
                if request.user.has_business():
                
                    instance.business = request.user.business.first()

            instance.save()

            messages.success(request, 'Thank you for contacting us, our team will get in touch with you!', extra_tags="support_added")

            return redirect ("contact")
    
    context = {
        "form": form,
    }

    return render(request, "support/contact.html", context )

@login_required
def support_list(request):

    if request.user.is_staff:
        support_requests = SupportRequest.objects.all()

        context = {
            "support_requests": support_requests,
        }

        return render(request, "support/admin/list.html", context )

    else:
        raise Http404


@login_required
def support_detail(request, pk):

    if request.user.is_staff:

        support_request = get_object_or_404(SupportRequest, pk=pk)
        support_request_replies = support_request.support_requests_reply.order_by('created').all()

        if request.method != "POST":
            form = SupportRequestReplyForm()
        else:
            form = SupportRequestReplyForm(request.POST or None)
            if form.is_valid():
                instance = form.save(commit=False)
                user = request.user
                instance.user = user
                if not user.is_staff:
                    if user.has_business():
                        instance.business = user.business.first()
                instance.name = user.get_full_name()
                instance.email = user.email
                instance.support_request = support_request
                instance.save()

                messages.success(request, 'Reply added successfully!', extra_tags="reply_added")

                url = reverse('support_detail', kwargs={'pk': support_request.pk})
                return HttpResponseRedirect(url)
            else:
                print("is not valid")
            
        context = {
                "support_request": support_request,
                "form": form,
                "support_request_replies": support_request_replies
            }

        return render(request, "support/admin/detail.html", context )

    else:
        raise Http404
