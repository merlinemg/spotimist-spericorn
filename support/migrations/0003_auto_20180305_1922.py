# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-03-05 19:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0002_auto_20180305_1500'),
    ]

    operations = [
        migrations.AlterField(
            model_name='supportrequest',
            name='email',
            field=models.CharField(default='fabio.radwan@gmail.com', max_length=250),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='supportrequest',
            name='name',
            field=models.CharField(default='fabio.radwan@gmail.com', max_length=250),
            preserve_default=False,
        ),
    ]
