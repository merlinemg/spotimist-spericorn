from django.contrib import admin
from .models import SupportRequest, SupportRequestReply

class PropertySupportRequestReplyInLine(admin.TabularInline):
    model = SupportRequestReply
    extra = 0

class SupportRequestModelAdmin(admin.ModelAdmin):
    list_display = ["subject", "created"]
    list_filter = ['business']
    search_fields = ["subject", "message"]
    inlines = [PropertySupportRequestReplyInLine]
    class Meta:
        model = SupportRequest

class SupportRequestReplyModelAdmin(admin.ModelAdmin):
    list_display = ["message", "user", "created"]
    search_fields = ["message"]
    class Meta:
        model = SupportRequestReply

admin.site.register(SupportRequest, SupportRequestModelAdmin)
# admin.site.register(SupportRequestReply, SupportRequestReplyModelAdmin)