from django import forms
from .models import SupportRequest, SupportRequestReply
from django.utils.translation import gettext_lazy as _

class SupportRequestForm(forms.ModelForm):
    class Meta:
        model = SupportRequest
        fields = [
            "subject",
            "message",
            "name",
            "email",
            ]
        
        widgets = {
            "name": forms.TextInput(attrs={'placeholder': 'Name'}),
            "email": forms.TextInput(attrs={'placeholder': 'Email', 'pattern': '^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$'}),
            "subject": forms.TextInput(attrs={'placeholder': 'Subject'}),
            "message": forms.Textarea(attrs={'placeholder': 'Message'}),
        }

class SupportRequestReplyForm(forms.ModelForm):
    class Meta:
        model = SupportRequestReply
        fields = [
            "message",
            ]
        
        widgets = {
            "message": forms.Textarea(attrs={'placeholder': 'Reply'}),
        }