from django.db import models
from django.conf import settings
from business.models import Business

# Create your models here.

class SupportRequest(models.Model):
    class Meta:
        verbose_name_plural = "support requests"
        ordering = ["-created"]
    subject = models.CharField(max_length=120)
    message = models.TextField()
    name = models.CharField(max_length=250)
    email = models.CharField(max_length=250)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="support_requests", blank=True, null=True)
    business = models.ForeignKey(Business, related_name="support_requests", blank=True, null=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)


class SupportRequestReply(models.Model):
    class Meta:
        verbose_name_plural = "support request reply"
        ordering = ["-created"]
    support_request = models.ForeignKey(SupportRequest, related_name="support_requests_reply")
    message = models.TextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="support_requests_reply", blank=True, null=True)
    business = models.ForeignKey(Business, related_name="support_requests_reply", blank=True, null=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)