"""
Here is where we declare our views driven by controllers
"""
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.template.loader import get_template

from services.models import RecurringService
from .forms import ActivityForm
from .models import Activity, OpeningHours
from reviews.models import Review
from reviews.forms import ReviewForm
from business.models import Business
from categories.models import Category
from django.conf import settings
import datetime


def home_page(request):
    queryset = Business.objects.filter(is_active=True)[:12]
    context = {
        "object_list": queryset,
        "api_url": reverse('api-business-list')
    }
    return render(request, "home.html", context)


def home_about_page(request):
    # queryset = Business.objects.filter(is_active=True)[:12]
    # context = {
    #     "object_list": queryset,
    #     "api_url": reverse('api-business-list')
    # }
    return render(request, "home-about.html")

@login_required
def dashboard(request):
    today = datetime.datetime.today()
    bookings = request.user.get_activities_booked().filter(date_from__gte=today).order_by('date_from')
    queryset = Business.objects.filter(is_active=True)[:12]

    reviews = Review.objects.filter(user=request.user).count()

    context = {
        "bookings": bookings[:2],
        "bookings_count": bookings.count(),
        "reviews_count": reviews,

    }
    return render(request, "activity/dashboard.html", context)

@login_required
def dashboard_business(request):
    business = request.user.business.first()

    activities_count = Activity.objects.filter(business=business).count()
    bookings_count = business.bookings.count()

    activities_count = activities_count
    bookings_count = bookings_count
    current_date = datetime.datetime.now().date()
    alert_business = Business.objects.get(user=request.user)
    alert_activity = Activity.objects.filter(business=alert_business)
    activity_array = []
    # for alert in alert_activity:
    #     alert_service = RecurringService.objects.filter(activity=alert, end_repeat__lt=current_date)
    #     if alert_service.exists():
    #         activity_array.append(alert_service)
    # print(activity_array, "11111111111")
    for alert in alert_activity:
        for i in RecurringService.objects.filter(activity=alert):
            array = []
            temp_array = []
            end_repeat = str(i.end_repeat)
            for end in end_repeat.split(','):
                if end not in temp_array:
                    array.append(end)
                    temp_array.append(end)
            try:
                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
            except:
                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
            for s_array in sort_array[-1:]:
                try:
                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                except:
                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                if n_date < current_date:
                    activity_array.append(i)

    context = {
        "activities_count": activities_count,
        "bookings_count": bookings_count,
        "reviews_count": business.get_reviews_count(),
        "alert_service": activity_array,
        "activities": alert_activity
    }
    return render(request, "activity/dashboard-business.html", context)


def blog_page(request):
    context = {
        "linkedin_url": settings.LINKEDIN_URL,
        "facebook_url": settings.FACEBOOK_URL
    }

    return render(request, "blog.html", context)


def blog_post(request):
    context = {
        "linkedin_url": settings.LINKEDIN_URL,
        "facebook_url": settings.FACEBOOK_URL
    }

    return render(request, "blog-post1.html", context)


def contact_page(request):
    return render(request, "contact.html")


def home_page_v2(request):
    queryset = Business.objects.filter(is_active=True)
    current_date = datetime.datetime.now().date()
    temp_location_array = []
    location_array = []
    for buss in queryset:
        get_activity = Activity.objects.filter(business=buss)
        if get_activity:
            for i in get_activity:
                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                    array = []
                    temp_array = []
                    end_repeat = str(i.end_repeat)
                    for end in end_repeat.split(','):
                        if end not in temp_array:
                            array.append(end)
                            temp_array.append(end)
                    try:
                        sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                    except:
                        sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                    for s_array in sort_array[-1:]:
                        try:
                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                        except:
                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                        if n_date >= current_date:
                            if buss not in temp_location_array:
                                location_array.append(buss)
                                temp_location_array.append(buss)
    queryset = location_array

    categories = Category.objects.filter(active=True)
    context = {
        "object_list": queryset[:12],
        "categories": categories
    }
    return render(request, "home.html", context)


def get_listed(request):
    queryset = Business.objects.filter(is_active=True)[:12]
    categories = Category.objects.filter(active=True)
    context = {
        "object_list": queryset,
        "categories": categories
    }
    return render(request, "get-listed.html", context)


@login_required
def activities_create(request):
    form = ActivityForm(request.POST or None, request.FILES or None)
    business = get_object_or_404(Business, user=request.user)
    if form.is_valid():
        instance = form.save(commit=False)
        # instance.user = request.user
        instance.business = business
        instance.latitude = business.latitude
        instance.longitude = business.longitude
        instance.address = business.address
        instance.postCode = business.postCode
        instance.city = business.city
        instance.country = business.country
        instance.save()
        form.save_m2m()
        business.set_status(True)
        # message success
        messages.success(request, "Successfully Created")
        return redirect('activities:my-list')
    # else:
    #     messages.error(request, "Not Successfully Created")
    context = {
        "form": form,
    }
    return render(request, "activity/create.html", context)


def activities_detail(request, slug):
    activity = get_object_or_404(Activity, slug=slug)
    review_form = ReviewForm(request.POST or None)
    if review_form.is_valid():
        instance = review_form.save(commit=False)
        instance.user = request.user
        instance.activity = activity
        instance.save()
        return HttpResponseRedirect(activity.get_absolute_url())

    reviews = Review.objects.filter(activity__id=activity.id)
    opening_hours = OpeningHours.objects.filter(activity__id=activity.id)
    context = {
        "activity": activity,
        "reviews": reviews,
        "opening_hours": opening_hours,
        "form": review_form,
    }

    return render(request, "activity/detail.html", context)


def activities_list(request):
    queryset_list = Activity.objects.all()

    paginator = Paginator(queryset_list, 10)  # Show 25 activies per page
    page_request_var = "page"
    page = request.GET.get(page_request_var)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)

    context = {
        "object_list": queryset,
        "title": "Activities List",
        "page_request_var": page_request_var,
        "activities_count": queryset_list.count()
    }

    return render(request, "activity/index.html", context)


@login_required
def my_activities_list(request):
    if request.user.has_business():
        business = Business.objects.get(user=request.user)
        queryset_list = Activity.objects.filter(business=business)

        paginator = Paginator(queryset_list, 10)  # Show 25 activies per page
        page_request_var = "page"
        page = request.GET.get(page_request_var)
        try:
            queryset = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            queryset = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            queryset = paginator.page(paginator.num_pages)

        context = {
            "object_list": queryset,
            "title": "Activities List",
            "page_request_var": page_request_var
        }

        return render(request, "activity/my_listings.html", context)
    else:
        return render(request, "activity/my_listings.html", context)


@login_required
def activities_update(request, slug):
    activity = get_object_or_404(Activity, slug=slug)

    if not activity.is_editable_by(request.user):
        raise Http404("You can't access this object.")

    form = ActivityForm(request.POST or None, request.FILES or None, instance=activity)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        form.save_m2m()
        # message success
        messages.success(request, "Successfully Updated", extra_tags="some-tag")
        return redirect("activities:my-list")
    # else:
    #     messages.error(request, "Not Successfully Updated")

    context = {
        "activity": activity,
        "form": form
    }

    return render(request, "activity/edit.html", context)


@login_required
def activities_delete(request, slug):
    activity = get_object_or_404(Activity, slug=slug)
    activity.delete()
    messages.success(request, "Successfully Deleted")
    return redirect("activities:my-list")


def activities_map(request):
    activities = Activity.objects.all().values('name', 'latitude', 'longitude', 'content',
                                               'marker_content')  # or simply .values() to get all fields
    activities_list = list(activities)  # important: convert the QuerySet to a list object
    return JsonResponse(activities_list, safe=False)


@login_required
def dashboard_user_activities(request):
    today = datetime.datetime.today()
    
    sort_query = request.GET.get('sort', '')

    if sort_query == "upcoming":
        bookings = request.user.get_activities_booked().filter(date_from__gte=today).order_by('date_from')
    elif sort_query == "past":
        bookings = request.user.get_activities_booked().filter(date_from__lte=today).order_by('-date_from')
    elif sort_query == "all":
        bookings = request.user.get_activities_booked().order_by('-date_from')
    else:
        bookings = request.user.get_activities_booked().filter(date_from__gte=today).order_by('date_from')
    
    context = {
        "bookings": bookings,
        "title": "Activities List",

    }

    return render(request, "activity/my_listings-user.html", context)
