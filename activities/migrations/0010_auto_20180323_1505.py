# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-03-23 15:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0009_auto_20180306_1851'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='business',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='activities', to='business.Business'),
        ),
    ]
