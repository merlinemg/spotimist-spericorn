"""
URLs configuration for Activities 

"""
from django.conf.urls import url

from . import views
from services.views import services_create

urlpatterns = [
    # ur    l(r'^$', views.activities_list, name="list"),
    url(r'^my-activities/$', views.my_activities_list, name="my-list"),
    url(r'^create/$', views.activities_create, name="create"),
    url(r'^get-activities-map/$', views.activities_map, name="get_activities_map"),
    # url(r'^(?P<slug>[\w-]+)/$', views.activities_detail, name="detail"),
    url(r'^(?P<slug>[\w-]+)/edit$', views.activities_update, name="edit"),
    url(r'^(?P<slug>[\w-]+)/delete/$', views.activities_delete, name="delete"),
    # url(r'^(?P<slug>[\w-]+)/services/create$', services_create, name="service-create"),
]
