"""
Activity Model configuration

"""

from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import pre_save, post_save, m2m_changed
from django.dispatch import receiver
from django.utils.text import slugify
from django.conf import settings
from django.template import Context, loader
from django.template.loader import get_template
from django.utils.translation import gettext as _
from business.models import Business
from services.models import RecurringService

WEEKDAYS = [
    (1, _("Monday")),
    (2, _("Tuesday")),
    (3, _("Wednesday")),
    (4, _("Thursday")),
    (5, _("Friday")),
    (6, _("Saturday")),
    (7, _("Sunday")),
]


# class Category(models.Model):
#     class Meta:
#         verbose_name_plural = "categories"
#         ordering = ["title"]
#     title = models.CharField(max_length=120)
#     description = models.CharField(max_length=250, blank=True, null=True)
#     css_class = models.CharField(max_length=120, blank=True, null=True)
#     slug = models.SlugField(unique=True, editable=False)
#     def __str__(self):
#         return self.title

# def create_category_slug(instance, new_slug=None):
#     print ("test")
#     slug = slugify(instance.title)
#     if new_slug is not None:
#         slug = new_slug
#     qs = Category.objects.filter(slug=slug).order_by("-id")
#     exists = qs.exists()
#     if exists:
#         new_slug = "%s-%s" %(slug, qs.first().id)
#         return create_category_slug(instance, new_slug=new_slug)
#     return slug

# def pre_save_category_receiver(sender, instance, *args, **kwargs):
#     if not instance.slug:
#         instance.slug = create_category_slug(instance)

# pre_save.connect(pre_save_category_receiver, sender=Category)

class Activity(models.Model):
    class Meta:
        verbose_name_plural = "activities"
        ordering = ["-created"]

    name = models.CharField(max_length=120)
    # user = models.ForeignKey(settings.AUTH_USER_MODEL)
    business = models.ForeignKey(Business, related_name="activities")
    description = models.TextField()
    content = models.TextField(default="")
    marker_content = models.TextField(default="")
    image = models.ImageField(blank=True, null=True, width_field="width_field", height_field="height_field")
    height_field = models.IntegerField(default=0, null=True)
    width_field = models.IntegerField(default=0, null=True)
    address = models.CharField(max_length=250, blank=True, null=True)
    postCode = models.CharField(max_length=120, blank=True, null=True)
    city = models.CharField(max_length=120, blank=True, null=True)
    country = models.CharField(max_length=120, blank=True, null=True)
    website = models.CharField(max_length=120, blank=True, null=True)
    phone = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=120, blank=True, null=True)
    facebookUrl = models.CharField(max_length=250, blank=True, null=True)
    twitterUrl = models.CharField(max_length=250, blank=True, null=True)
    instagramUrl = models.CharField(max_length=250, blank=True, null=True)
    youtubeUrl = models.CharField(max_length=250, blank=True, null=True)
    linkedInUrl = models.CharField(max_length=250, blank=True, null=True)
    latitude = models.CharField(max_length=120, blank=True, null=True)
    longitude = models.CharField(max_length=120, blank=True, null=True)
    slug = models.SlugField(unique=True, editable=False)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    categories = models.ManyToManyField('categories.Category')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return ""  # reverse ("activities:detail", kwargs={"slug": self.slug})

    def get_edit_url(self):
        return reverse("activities:edit", kwargs={"slug": self.slug})

    def get_delete_url(self):
        return reverse("activities:delete", kwargs={"slug": self.slug})

    def get_services_url(self):
        return reverse("services-list", kwargs={"slug": self.slug})

    def get_services_list(self):
        services = RecurringService.objects.filter(activity=self, is_archived=False)
        return services

    def get_image_url(self):
        if self.image:
            image_url = self.image.url
        else:
            image_url = settings.STATIC_URL + "Old/img/placeholder_2.jpg"

        return image_url

    def is_editable_by(self, user):
        return self.business.user == user

    def has_services(self):
        service_count = RecurringService.objects.filter(activity=self, is_archived=False).count()
        if service_count > 0:
            return True
        else:
            return False

class OpeningHours(models.Model):
    weekday = models.IntegerField(choices=WEEKDAYS)
    from_hour = models.TimeField()
    to_hour = models.TimeField()
    activity = models.ForeignKey(Activity)

    class Meta:
        verbose_name_plural = "opening hours"
        ordering = ('weekday', 'from_hour')
        unique_together = ('weekday', 'from_hour', 'to_hour')

    def __str__(self):
        return u'%s: %s - %s' % (self.get_weekday_display(), self.from_hour, self.to_hour)


def create_activity_slug(instance, new_slug=None):
    slug = slugify(instance.name)
    if new_slug is not None:
        slug = new_slug
    qs = Activity.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return create_activity_slug(instance, new_slug=new_slug)
    return slug


@receiver(pre_save, sender=Activity)
def pre_save_activity_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_activity_slug(instance)


# we need to use the m2m_changed signal because the post_save is called BEFORE the m2m relationships get updated
@receiver(m2m_changed, sender=Activity.categories.through)
def update_business_categories(sender, instance, action, **kwargs):
    if (action == "post_add" or action == "post_remove"):
        business = instance.business
        # clearing all categories previously assigned to business
        business.categories.clear()

        categories = Activity.objects.filter(business=business).values("categories")

        cat_list = []
        for cat in categories:
            cat_list.append(cat['categories'])

        cat_list = set(cat_list)
        for cat in cat_list:
            business.categories.add(int(cat))

# """ 
# This receiver is triggered every time the activity is saved
# The reason is that before the saving process the image hasn't been uploaded to filesystem there for image name will reflect the name of the file the user is uploading 
# rather than the actual name that the application will store on file system.
# Also, we need to make sure that everytime the activity is updated then we update the marker content value otherwise the map will keep displaying old data (e.g. Image/name)
# """
# @receiver(post_save, sender=Activity)
# def marker_handler(sender, **kwargs):
#     activity = kwargs.get('instance')

#     ## Reading the template that needs to be injected in the markers to be displayed in the map
#     template_string = get_template('activity/marker_content.html').render()
#     content = template_string.replace('[[name]]', activity.name).replace('[[link]]', activity.get_absolute_url()).replace('[[image]]', activity.get_image_url())
#     marker_content = '<div class="marker-inventor-poi"><div class="marker-inventor-poi-inner"><i class="inventor-poi inventor-poi-house"></i></div></div>'

#     if kwargs.get('created', False):
#         activity.content = content
#         activity.marker_content = marker_content
#         activity.save()
#     else:
#         activity.content = template_string.replace('[[name]]', activity.name).replace('[[link]]', activity.get_absolute_url()).replace('[[image]]', activity.get_image_url())
#         marker_content = '<div class="marker-inventor-poi"><div class="marker-inventor-poi-inner"><i class="inventor-poi inventor-poi-house"></i></div></div>'
#         Activity.objects.filter(id=activity.id).update(content=content, marker_content=marker_content)

# # def save_marker_content(sender, instance, created, **kwargs):
# #     if created:
# #         template_string = get_template('marker_content.html').render()
# #         instance.content = template_string.replace('[[name]]', instance.name).replace('[[link]]', instance.get_absolute_url()).replace('[[image]]', instance.get_image_url())
# #         instance.marker_content = '<div class="marker-inventor-poi"><div class="marker-inventor-poi-inner"><i class="inventor-poi inventor-poi-house"></i></div></div>'
# #         instance.save()

# pre_save.connect(pre_save_activity_receiver, sender=Activity)
# # post_save.connect(save_marker_content, sender=Activity)
