"""
Here is where we customize the Activity for the Admin area
"""
from django.contrib import admin
from .models import Activity, OpeningHours
from services.models import RecurringService

# Register your models here.

class PropertyServiceInLine(admin.TabularInline):
    model = RecurringService
    extra = 0

class ActivityModelAdmin(admin.ModelAdmin):
    list_display = ["name", "business", "created"]
    list_filter = ["business", "name", "is_active"]
    search_fields = ["name", "description"]
    inlines = [PropertyServiceInLine]
    # list_oder = ["business"]
    class Meta:
        model = Activity


# class CategoryModelAdmin(admin.ModelAdmin):
#     list_display = ["title"]
#     list_filter = ['title']
#     search_fields = ["title", "description"]
#     class Meta:
#         model = Category

class OpeningHoursModelAdmin(admin.ModelAdmin):
    list_display = ["activity", "weekday"]
    list_filter = ['activity']
    search_fields = ["activity"]

    class Meta:
        model = OpeningHours


admin.site.register(Activity, ActivityModelAdmin)
# admin.site.register(Category, CategoryModelAdmin)
admin.site.register(OpeningHours, OpeningHoursModelAdmin)
