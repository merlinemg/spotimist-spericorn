from .models import Activity
from rest_framework import routers, serializers, viewsets, generics
from services.serializers import RecurringServiceSerializer


# Serializers define the API representation.
class ActivitySerializer(serializers.HyperlinkedModelSerializer):
    recurringservice_set = RecurringServiceSerializer(many=True, read_only=True)

    class Meta:
        model = Activity
        fields = ['id', 'url', 'name', 'city', 'description', 'slug', 'image', 'recurringservice_set', 'business']


# ViewSets define the view behavior.
class ActivityViewSet(viewsets.ModelViewSet):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer


class ActivityList(generics.ListAPIView):
    serializer_class = ActivitySerializer

    def get_queryset(self):
        query_set = Activity.objects

        business_query = self.request.query_params.get('business', None)
        if business_query is not None:
            query_set = query_set.filter(business=business_query)

        return query_set
