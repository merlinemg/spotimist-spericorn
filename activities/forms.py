from django import forms
from .models import Activity


class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = [
            "name",
            "description",
            "image",
            "email",
            "website",
            "phone",
            # "latitude",
            # "longitude",
            "categories",
            # "address",
            # "postCode",
            # "city",
            # "country"
        ]
        widgets = {
            "name": forms.TextInput(attrs={'class': 'form-control'}),
            "description": forms.Textarea(attrs={'class': 'form-control', 'cols': '5', 'rows': '5'}),
            "email": forms.EmailInput(attrs={'class': 'form-control'}),
            "website": forms.TextInput(attrs={'class': 'form-control'}),
            "phone": forms.TextInput(attrs={'class': 'form-control'}),
            "categories": forms.CheckboxSelectMultiple(),
            # "latitude": forms.HiddenInput(),
            # "longitude": forms.HiddenInput(),
            # "categories": forms.CheckboxSelectMultiple(),
            # "address": forms.HiddenInput(),
            # "postCode": forms.HiddenInput(),
            # "city": forms.HiddenInput(),
            # "country": forms.HiddenInput(),
        }

    def clean_description(self):
        description = self.cleaned_data.get('description')
        description_length = 150
        if len(description) < description_length:
            raise forms.ValidationError("Description content should be minimum 150 characters...!")
        return description
