"""spotimist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from business import views
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from activities.views import (
    home_page,
    home_about_page,
    dashboard,
    dashboard_business,
    dashboard_user_activities,
    blog_page,
    blog_post,
    get_listed,
    home_page_v2
)

from business.views import business_detail, dashboard_get_app
from business.serializers import BusinessViewSet, BusinessList, BusinessDetail, CategoryViewSet
from services.models import RecurringService, Occurrence
from services.views import services_list, services_create, services_edit, services_delete, occurences_json

from bookings.serializers import BookingDetail, BookingItemDetail

from activities.models import Activity, OpeningHours
from activities.seralizers import ActivityViewSet
from services.serializers import RecurringServiceViewSet
from rest_framework import routers, serializers, viewsets, generics

from support.views import contact_form, support_list, support_detail


# Routers provide an easy way of automatically determining the URL conf.

router = routers.DefaultRouter()
# router.register(r'users', UserViewSet)
router.register(r'activities', ActivityViewSet)
# router.register(r'businesses', BusinessViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'recurring-services', RecurringServiceViewSet)
# router.register(r'openinghours', OpeningHoursViewSet)

if settings.ENABLE_TWO_FACTOR_ADMIN:
    from django_otp.admin import OTPAdminSite
    admin.site.__class__ = OTPAdminSite

urlpatterns = [
    url(r'^$', home_page_v2, name='home'),
    
    url(r'^accounts/', include('allauth.urls')),
    url(r'^profile/', include("accounts.urls", namespace="profile")),
    url(r'^home-v2/', home_page, name='home-v2'),
    url(r'^home-about/', home_about_page, name='home-about'),
    url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^dashboard-business/$', dashboard_business, name='dashboard-business'),
    url(r'^dashboard-business/get-app/', dashboard_get_app, name='dashboard-get-app'),
    url(r'^dashboard/my-activities/$', dashboard_user_activities, name='dashboard_user_activities'),
    url(r'^blog/', blog_page, name='blog'),
    url(r'^spotimist-day-1/', blog_post, name='blog_post'),
    url(r'^dashboard/support/$', support_list, name='support_list'),
    url(r'^dashboard/support/(?P<pk>[0-9]+)/$', support_detail, name='support_detail'),
    url(r'^contact/', contact_form, name='contact'),
    url(r'^get-listed/', get_listed, name='get-listed'),

    url(r'^activities/', include("activities.urls", namespace="activities")),
    url(r'^bookings/', include("bookings.urls", namespace="bookings")),
    url(r'^business/', include("business.urls", namespace="business")),
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'^spotmin/', admin.site.urls),
    url(r'^privacy-policy/', TemplateView.as_view(template_name="privacy_policy.html"), name='privacy'),
    url(r'^terms-and-conditions/', TemplateView.as_view(template_name="terms_and_conditions.html"), name='tandc'),
    url(r'^booking-terms-and-conditions/', TemplateView.as_view(template_name="booking_terms_and_conditions.html"),
        name='btandc'),

    url(r'api/businesses/$', BusinessList.as_view(), name="api-business-list"),
    url(r'api/business-detail/(?P<pk>[0-9]+)/$', BusinessDetail.as_view(), name="business-detail"),
    url(r'api/booking-detail/(?P<booking_id>\w+)/(?P<business_id>[0-9]+)/$', BookingDetail.as_view(),
        name="booking-detail"),
    url(r'api/bookingitem-detail/(?P<pk>[0-9]+)/$', BookingItemDetail.as_view(), name="bookingitem-detail"),
    url(r'api/', include(router.urls)),
    url(r'^rest-auth/', include('rest_auth.urls')),

    url(r'listings/', views.FilterView.as_view(), name="businesses-list"),
    url(r'^dashboard/reviews/', include("reviews.urls", namespace="reviews")),

    url(r'^(?P<slug>[\w-]+)/$', business_detail, name="business-detail"),
    url(r'^activities/(?P<slug>[\w-]+)/services/$', services_list, name="services-list"),
    url(r'^services/calendar-json/$', occurences_json, name="occurences-json"),
    url(r'^activities/(?P<slug>[\w-]+)/services/create/', services_create, name="services-add"),
    url(r'^activities/(?P<slug>[\w-]+)/services/(?P<id>[0-9]+)/edit/', services_edit, name="services-edit"),
    url(r'^activities/(?P<slug>[\w-]+)/services/(?P<id>[0-9]+)/delete/', services_delete, name="services-delete"),
    ## Not needed for now, maybe in the future
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^spotimist/', include("spotimist_api.urls", namespace="spotimist-api")),
]

# When going into production we need to enable staticfiles collection into a separate folder/server
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns
