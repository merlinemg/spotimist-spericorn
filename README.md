# Spotimist-Django

Project created on 23/02/2017 on:
- Python 3.6.0
- Django 1.10.5

When setting up a virtual environment run **pip install -r requirements.txt**

**Load initial data**:
python manage.py loaddata spotimist.json

This command will create a few categories, an activity and 3 users:
- user: fabio.radwan@spotimist.com, password = spotimist
- user: florent.serra@spotimist.com, password = spotimist 