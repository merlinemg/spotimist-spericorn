# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-04 10:21
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, height_field='height_field', null=True, upload_to='', width_field='width_field')),
                ('height_field', models.IntegerField(default=0)),
                ('width_field', models.IntegerField(default=0)),
                ('city', models.CharField(blank=True, max_length=120, null=True)),
                ('country', models.CharField(blank=True, max_length=120, null=True)),
                ('facebookUrl', models.CharField(blank=True, max_length=250, null=True)),
                ('twitterUrl', models.CharField(blank=True, max_length=250, null=True)),
                ('instagramUrl', models.CharField(blank=True, max_length=250, null=True)),
                ('youtubeUrl', models.CharField(blank=True, max_length=250, null=True)),
                ('linkedInUrl', models.CharField(blank=True, max_length=250, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
