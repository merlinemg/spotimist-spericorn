from django import forms

from django.contrib.auth.forms import ReadOnlyPasswordHashField
from allauth.account.forms import LoginForm
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now
from django.contrib.auth.password_validation import validate_password
from allauth.account.utils import setup_user_email
from .models import Profile

User = get_user_model()


class SignupForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email"]
        widgets = {
            "first_name": forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}),
        }

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        validate_password(password1)
        return password2

    def signup(self, request, user):

        print ("here I sign up")

        # user.first_name = self.cleaned_data['first_name']
        # user.last_name = self.cleaned_data['last_name']
        # user.save()

    def save(self, commit=True):

        print("Here I save")
        # Save the provided password in hashed format
        user = super(SignupForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        from allauth.account.models import EmailAddress, EmailConfirmation, EmailConfirmationHMAC

        if commit:
            user.save()

        # Creating both email confirmation in the db and in the console - To be changed later 
        email_address = EmailAddress.objects.create(user=user, email=user.email, primary=True, verified=False)
        email_address.save()

        confirmation = EmailConfirmation.create(email_address)
        confirmation.sent = now()
        confirmation.save()

        confirmationHMAC = EmailConfirmationHMAC(email_address)
        confirmationHMAC.send()

        return user


# class CoreLoginForm(LoginForm):

#     def __init__(self, *args, **kwargs):
#         super(CoreLoginForm, self).__init__(*args, **kwargs)
#         ## here i add the new fields that i need
#         self.fields["email"] = forms.CharField(label=_('E-mail'), max_length=100,widget=forms.TextInput(
#                                    attrs={'placeholder': _('E-mail')}))


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    # password = ReadOnlyPasswordHashField()
    # password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    # password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email"]
        widgets = {
            "first_name": forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}),
        }

    # def clean_password(self):
    #     # Regardless of what the user provides, return the initial value.
    #     # This is done here, rather than on the field, because the
    #     # field does not have access to the initial value
    #     # return self.initial["password"]
    #     print("clean password")
    #     # password1=self.cleaned_data.get('new_password')
    #     # password2=self.cleaned_data.get('reenter_password')

    #     return self.cleaned_data #don't forget this.


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = [
            "image",
            "city",
            "country",
        ]
