from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.user_detail, name="user-detail"),
    url(r'^change-psw/$', views.change_psw, name="change-psw"),
    url(r'^upload-profile-picture/$', views.upload_profile_picture, name="upload-profile-image"),
]