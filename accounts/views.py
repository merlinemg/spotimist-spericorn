from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render
from django.core.urlresolvers import reverse
from .models import CustomUser, Profile
from .forms import ProfileForm, SignupForm, UserChangeForm
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm


# Create your views here.

def user_detail(request):
    user = get_object_or_404(CustomUser, id=request.user.id)
    profile = get_object_or_404(Profile, user=user)

    profile_form = ProfileForm(request.POST or None, request.FILES or None, instance=profile)
    user_form = UserChangeForm(request.POST or None, request.FILES or None, instance=user)

    psw_form = PasswordChangeForm(request.user, prefix="pswfor")

    if request.method == 'POST':
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()

            return HttpResponseRedirect(request.get_full_path())
        else:

            print("not valid")
            print(user_form.errors)
            print(profile_form.errors)

    context = {
        "user": user,
        "profile": profile,
        "user_form": user_form,
        "profile_form": profile_form,
        'psw_form': psw_form
    }

    return render(request, "profile/profile.html", context)


def change_psw(request):
    if request.method == 'POST':
        psw_form = PasswordChangeForm(request.user, request.POST, prefix="pswfor")
        if psw_form.is_valid():
            user = psw_form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!', extra_tags="password_changed")
            return redirect("profile:user-detail")
        else:
            messages.error(request, 'Please correct the error below.')
            user = get_object_or_404(CustomUser, id=request.user.id)
            profile = get_object_or_404(Profile, user=user)

            profile_form = ProfileForm(request.POST or None, request.FILES or None, instance=profile)
            user_form = UserChangeForm(request.POST or None, request.FILES or None, instance=user)

            return render(request, 'profile/profile.html', {
                "user": user,
                "profile": profile,
                "user_form": user_form,
                "profile_form": profile_form,
                "psw_form": psw_form
            })

    else:
        raise Http404

def upload_profile_picture(request):
    profile = get_object_or_404(Profile, user=request.user)
    
    try:
        files = [request.FILES.get('file')]
        
        for f in files:
            profile.image = f
            profile.save()

        return HttpResponse("OK")
    except Exception as e:
        print("error in upload " + str(e))
