from django.conf import settings
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, Permission, Group, PermissionsMixin
)
from django.core import serializers
from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from business.models import Business

from allauth.socialaccount.models import SocialAccount


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = "user"

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    user_token = models.CharField(max_length=200, null=True, blank=True)

    # REQUIRED_FIELDS = ['date_of_birth']

    def get_full_name(self):
        # The user is identified by their email address
        return "{} {}".format(self.first_name, self.last_name)

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_business(self):
        user_business = False
        try:
            business = Business.objects.get(user=self)
            user_business = True
        except ObjectDoesNotExist:
            user_business = False

        return user_business

    def get_business_url(self):
        business = Business.objects.get(user=self)
        return business.get_absolute_url()

    def get_activities_booked(self):
        return self.bookings.all().prefetch_related('booking_items')

    def is_social(self):
        user_is_social = False
        try:
            social_account = SocialAccount.objects.get(user=self)
            user_is_social = True
        except ObjectDoesNotExist:
            user_is_social = False

        return user_is_social

    @property
    def business_id(self):

        if self.has_business():
            business = Business.objects.get(user=self)
            return business.id
        else:
            return -1

    # @property
    # def business(self):
    #     business_obj = Business.objects.filter(user=self.pk).values_list('name', 'addressGmap')

    #     return business_obj

    # def has_perm(self, perm, obj=None):
    #     "Does the user have a specific permission?"
    #     # Simplest possible answer: Yes, always
    #     return True

    # def has_module_perms(self, app_label):
    #     "Does the user have permissions to view the app `app_label`?"
    #     # Simplest possible answer: Yes, always
    #     return True

    # @property
    # def is_staff(self):
    #     "Is the user a member of staff?"
    #     # Simplest possible answer: All admins are staff
    #     return self.is_admin


# Extending with Profile

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    image = models.ImageField(blank=True, null=True, width_field="width_field", height_field="height_field")
    height_field = models.IntegerField(default=0, null=True)
    width_field = models.IntegerField(default=0, null=True)
    city = models.CharField(max_length=120, blank=True, null=True)
    country = models.CharField(max_length=120, blank=True, null=True)
    facebookUrl = models.CharField(max_length=250, blank=True, null=True)
    twitterUrl = models.CharField(max_length=250, blank=True, null=True)
    instagramUrl = models.CharField(max_length=250, blank=True, null=True)
    youtubeUrl = models.CharField(max_length=250, blank=True, null=True)
    linkedInUrl = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return str(self.user.email)


def post_save_user_model_receiver(sender, instance, created, *args, **kwargs):
    if created:
        try:
            Profile.objects.create(user=instance)
            # ActivationProfile.objects.create(user=instance)
        except:
            pass


post_save.connect(post_save_user_model_receiver, sender=settings.AUTH_USER_MODEL)


class SocialUser(models.Model):
    email = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='social_user')
    socialmedia_id = models.CharField(max_length=200, unique=True)
    social_type = models.CharField(max_length=200)

    def __str__(self):
        return self.socialmedia_id
