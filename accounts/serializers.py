from django.contrib.auth import get_user_model
from rest_framework import serializers, viewsets
from business.serializers import BusinessSerializer
from business.models import Business

User = get_user_model()


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'url', 'email', 'is_staff')


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CustomUserDetailsSerializer(serializers.ModelSerializer):
    # business_set = serializers.PrimaryKeyRelatedField(queryset=Business.objects.all())
    """
    User model w/o password
    """

    class Meta:
        model = User
        fields = ('pk', 'email', 'first_name', 'last_name', 'has_business', 'business_id')
        read_only_fields = ('email', 'first_name', 'last_name')
