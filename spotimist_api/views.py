from django.contrib.auth.hashers import check_password
from django.shortcuts import render

# Create your views here.
from rest_framework import generics, permissions, status, views
from rest_framework.authtoken.models import Token

from accounts.models import SocialUser, CustomUser
from business.models import Business
from spotimist_api import serializers
from rest_framework.response import Response


class SocialLoginAPIView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.SocialUserSerializer

    def create(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)
        try:
            try:
                get_social_user = SocialUser.objects.get(socialmedia_id=request.data['socialmedia_id'])
                get_id = get_social_user.email.id
                set_query = Business.objects.filter(user=get_social_user.email.id)
                user_obj = CustomUser.objects.get(email=get_social_user.email)
                token = ''
                if user_obj.is_active:
                    token, created = Token.objects.get_or_create(user=user_obj)
                bus_id = 0
                has_business = user_obj.has_business()
                if set_query:
                    for query in set_query:
                        bus_id = query.id
            except:
                get_social_user = CustomUser.objects.get(email__icontains=request.data['email'])
                get_id = get_social_user.id
                set_query = Business.objects.filter(user=get_social_user.id)
                user_obj = CustomUser.objects.get(email__icontains=get_social_user.email)
                token = ''
                if user_obj.is_active:
                    token, created = Token.objects.get_or_create(user=user_obj)
                bus_id = 0
                has_business = user_obj.has_business()
                if set_query:
                    for query in set_query:
                        bus_id = query.id
                # else:
                #     has_business = False
            if get_social_user:
                payload = {
                    "social_token": str(token),
                    "pk": get_id,
                    "email": str(get_social_user.email),
                    "first_name": request.data['first_name'],
                    "last_name": request.data['last_name'],
                    "has_business": has_business,
                    "business_id": bus_id

                }
                return Response(payload, status=status.HTTP_200_OK)
        except:
            main_user = CustomUser.objects.create_user(email=request.data['email'])
            main_user.first_name = request.data['first_name']
            main_user.last_name = request.data['last_name']
            main_user.save()
            create_social_user = SocialUser.objects.create(email=main_user,
                                                           socialmedia_id=request.data['socialmedia_id'],
                                                           social_type=request.data['social_type'])
            # create_social_user.social_token()
            create_social_user.save()
            token, created = Token.objects.get_or_create(user=main_user)
            payload = {
                "social_token": str(token),
                "pk": create_social_user.email.id,
                "email": str(create_social_user.email),
                "first_name": request.data['first_name'],
                "last_name": request.data['last_name'],
            }

            return Response(payload, status=status.HTTP_200_OK)


class LoginAPIView(views.APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.UserLoginSerializer

    def post(self, request):
        payload = ''
        serializer = self.serializer_class(data=request.data)
        try:
            user_obj = CustomUser.objects.get(email=request.data.get('email').lower())
            if user_obj.is_active:
                if not user_obj.check_password(request.data.get('password')):
                    payload = {
                        "details": "Password mismatch",
                    }
                else:
                    token, created = Token.objects.get_or_create(user=user_obj)
                    payload = {
                        "key": str(token),
                    }
        except:
            payload = {
                "details": "Not found",
            }
        return Response(payload, status=status.HTTP_200_OK)


class UserAPIView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        token_user = Token.objects.get(key=str(request.META.get('HTTP_AUTHORIZATION')).split()[1]).user
        user_pk = ''
        user_email = ''
        user_first_name = ''
        user_last_name = ''
        user_business = ''
        user_business_id = 0
        if token_user:
            user_pk = CustomUser.objects.get(email=token_user).id
            user_email = CustomUser.objects.get(email=token_user).email
            user_first_name = CustomUser.objects.get(email=token_user).first_name
            user_last_name = CustomUser.objects.get(email=token_user).last_name
            user_business = CustomUser.has_business(token_user)
            if user_business:
                user_business_id = Business.objects.get(user=token_user).id
        payload = {
            "pk": user_pk,
            "email": user_email,
            "first_name": user_first_name,
            "last_name": user_last_name,
            "has_business": user_business,
            "business_id": user_business_id
        }
        return Response(payload, status=status.HTTP_200_OK)
