from django.conf.urls import url

from . import views

urlpatterns = [

    url(r'^login/', views.LoginAPIView.as_view(), name='login'),
    url(r'^social-login/', views.SocialLoginAPIView.as_view(), name='social-login'),
    url(r'^user/', views.UserAPIView.as_view(), name='user'),
]

