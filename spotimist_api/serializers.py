from rest_framework import serializers
from rest_framework.authtoken.models import Token

from accounts.models import SocialUser, CustomUser


class SocialUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialUser
        fields = ('email', 'socialmedia_id', 'social_type')


class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('email', 'password')

    def validate(self, attrs):
        user_obj = CustomUser.objects.get(email=attrs.get('email'))
        if user_obj.is_active:
            token, created = Token.objects.get_or_create(user=user_obj)
            attrs['token'] = token
        return attrs
