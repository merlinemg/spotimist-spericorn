from django.apps import AppConfig


class SpotimistApiConfig(AppConfig):
    name = 'spotimist_api'
