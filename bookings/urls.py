"""URLs for the booking app."""
from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^(?P<pk>\d+)/$',views.BookingDetailView.as_view(),name='booking_detail'),
    url(r'^create/$',views.BookingCreateView.as_view(),name='booking_create'),
    url(r'^$', views.BookingListView.as_view(), name='booking_list'),
    url(r'^book/$', views.booking_landing, name='booking_landing'),
    url(r'^checkout/$', views.booking_create, name='booking_create'),
    url(r'^order-confirmed/$', views.booking_confirmation, name='booking_confirmation'),
    url(r'^transactions/$', views.transactions_history, name='booking_transaction'),
]