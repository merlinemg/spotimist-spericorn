#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""Views for the booking app."""
from decimal import Decimal

import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.contrib.sessions.models import Session
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.db import transaction

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, DetailView, ListView
from django.views.generic.base import TemplateView
from djstripe.models import Charge, Customer, Card
from djstripe.settings import get_idempotency_key

from spotimist import settings
from .forms import BookingForm, BookingFormCreate
from .models import Booking, BookingItem, TransactionHistory
from activities.models import Activity
from services.models import RecurringService
from spotimist.utils import current_site_url
from business.models import Business
import dateutil.parser
import stripe
from django.contrib import messages



# from realexpayments.domain import PaymentRequest, PaymentType, Card, CardType, Cvn, Amount, AutoSettle
# from realexpayments.client import RealexClient
# from realexpayments.utils import GenerationUtils

# ------ MIXINS ------ #

class BookingViewMixin(object):
    model = Booking
    form_class = BookingForm


# ------ MODEL VIEWS ------ #

class BookingCreateView(BookingViewMixin, CreateView):
    """View to create a new ``Booking`` instance."""

    def get_success_url(self):
        return reverse('booking_detail', kwargs={'pk': self.object.pk})

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(BookingCreateView, self).get_form_kwargs(
            *args, **kwargs)
        if self.request.user.is_authenticated():
            kwargs.update({'user': self.request.user})
        else:
            # If the user is not authenticated, get the current session
            if not self.request.session.exists(
                    self.request.session.session_key):
                self.request.session.create()
            kwargs.update({'session': Session.objects.get(
                session_key=self.request.session.session_key)})
        return kwargs


class BookingDetailView(BookingViewMixin, DetailView):
    """View to display a ``Booking`` instance."""

    def dispatch(self, request, *args, **kwargs):
        self.kwargs = kwargs
        self.object = self.get_object()
        if request.user.is_authenticated():
            # If user doesn't own the booking forbid access
            if not self.object.user == request.user:
                raise Http404
        else:
            # If anonymous doesn't own the booking forbid access
            session = self.object.session
            if (not session or not request.session.session_key or
                    session.session_key != request.session.session_key):
                raise Http404
        return super(BookingViewMixin, self).dispatch(request, *args, **kwargs)


class BookingListView(BookingViewMixin, ListView):
    template_name = "bookings/dashboard/booking_list.html"

    """View to display all ``Booking`` instances of one user."""

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(BookingViewMixin, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        business = Business.objects.get(user=self.request.user)
        # print(business.bookings.all())

        return business.bookings.all().prefetch_related('booking_items')


@login_required
def booking_landing(request):
    # import uuid 
    # print(uuid.uuid1())
    # print(uuid.uuid1().hex[:16].upper())

    # form = BookingForm(request.POST or None, request.FILES or None)
    data = {'forename': request.user.first_name, 'surname': request.user.last_name, 'email': request.user.email}
    form = BookingFormCreate(initial=data)

    if settings.STRIPE_LIVE_MODE:
        stripe_api_key = settings.STRIPE_LIVE_PUBLIC_KEY
    else:
        stripe_api_key = settings.STRIPE_TEST_PUBLIC_KEY

    if request.method == "POST":
        activity = request.POST.get('hdActivity')
        service = request.POST.get('hdService')
        date = request.POST.get('hdDate')
        get_time = request.POST.get('hdTime')
        time = datetime.datetime.strptime(get_time, '%H:%M:%S').strftime('%H:%M')
        guest = request.POST.get('qtyTotal')
        activity_obj = Activity.objects.get(id=activity)
        service_obj = RecurringService.objects.get(id=service)
        service_price = str(service_obj.price * int(guest))
        result = service_price.split('.')[1:]
        if str(result) == "['00']":
            service_price = int(service_obj.price * int(guest))
        temp_product = {
            "activity": activity,
            "service": service,
            "date": date,
            "time": time,
            "guest": guest,
            "price": service_price
        }
        request.session["temp_product"] = temp_product

    else:
        if "temp_product" in request.session:
            temp_product = request.session["temp_product"]
            activity = temp_product["activity"]
            service = temp_product["service"]
            date = temp_product["date"]
            time = temp_product["time"]
            guest = temp_product['guest']
            activity_obj = Activity.objects.get(id=activity)
            service_obj = RecurringService.objects.get(id=service)
            service_price = str(service_obj.price * int(guest))
            result = service_price.split('.')[1:]
            if str(result) == "['00']":
                service_price = int(service_obj.price * int(guest))
        else:
            # Get values from Cookies
            activity = request.COOKIES.get('activity')
            service = request.COOKIES.get('service')
            date = request.COOKIES.get('date')
            time = request.COOKIES.get('time')
            guest = request.COOKIES.get('guest')
            activity_obj = Activity.objects.get(id=activity)
            service_obj = RecurringService.objects.get(id=service)
            service_price = str(service_obj.price * int(guest))
            result = service_price.split('.')[1:]
            if str(result) == "['00']":
                service_price = int(service_obj.price * int(guest))
            temp_product = {
                "activity": activity,
                "service": service,
                "date": date,
                "time": time,
                "guest": guest,
                "price": service_price
            }

            request.session["temp_product"] = temp_product

    context = {
        "form": "",
        "activity": activity_obj,
        "service": service_obj,
        "date": date,
        "time": time,
        "guest": guest,
        "business": activity_obj.business,
        "form": form,
        "price": service_price,
        "stripe_api_key": stripe_api_key
    }

    return render(request, "bookings/booking_form.html", context)


@login_required
def booking_create(request):
    form = BookingFormCreate(request.POST or None)

    if "temp_product" in request.session:
        temp_product = request.session["temp_product"]
        activity = temp_product["activity"]
        service = temp_product["service"]
        date = temp_product["date"]
        time = temp_product["time"]
        guest = temp_product["guest"]

        activity_obj = Activity.objects.get(id=activity)
        service_obj = RecurringService.objects.get(id=service)
        service_price = float(service_obj.price * int(guest))

    booking_id = ""
    qr_image = ""

    if form.is_valid():

        instance = form.save(commit=False)
        instance.user = request.user
        instance.total = service_price
        # print(request.user.business.first())
        instance.business = activity_obj.business
        date = datetime.datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d')
        parse_date = date + " " + time
        instance.date_from = parse_date
        instance.save()

        booking_id = instance.booking_id
        qr_image = instance.qrcode.url

        booking_item = BookingItem()

        booking_item.quantity = 1
        booking_item.persons = guest
        booking_item.subtotal = service_price

        booking_item.content_type = ContentType.objects.get_for_model(service_obj)
        booking_item.object_id = service_obj.id
        booking_item.booking = instance

        business_instance = Business.objects.get(name=instance.business)
        booking_instance = Booking.objects.get(booking_id=instance.booking_id)

        try:
            if service_price != 0:
                # Use Stripe's library to make requests...
                if settings.STRIPE_LIVE_MODE:
                    stripe.api_key = settings.STRIPE_LIVE_SECRET_KEY
                else:
                    stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

                token = request.POST.get('stripeToken')
                customer = stripe.Customer.create(
                    description=request.user,
                    source=token,
                )
                charge = stripe.Charge.create(
                    amount=int(service_price * 100),
                    currency="EUR",
                    customer=customer
                )
                bal = stripe.BalanceTransaction.retrieve(charge.balance_transaction)
                stripe_fee = float(bal.fee) / float(100)

                if business_instance.booking_fee or business_instance.booking_fee == 0:
                    percentage = business_instance.booking_fee
                else:
                    percentage = service_obj.service_type.service_percentage

                price = float(service_price)
                result = (price * percentage) / 100
                total_fee = result + stripe_fee
                withdrawal_amnt = price - total_fee

                new_transaction = TransactionHistory(
                    transaction_business=business_instance,
                    transaction_booking=booking_instance,
                    transaction_withdraw=withdrawal_amnt,
                    transaction_amount=charge.amount / 100,
                    transaction_amount_refunded=charge.amount_refunded,
                    transaction_application=charge.application,
                    transaction_application_fee=charge.application_fee,
                    transaction_balance=charge.balance_transaction,
                    transaction_captured=charge.captured,
                    transaction_created=datetime.datetime.fromtimestamp(charge.created),
                    transaction_currency=charge.currency,
                    transaction_customer=charge.customer,
                    transaction_description=charge.description,
                    transaction_destination=charge.destination,
                    transaction_dispute=charge.dispute,
                    transaction_failure_code=charge.failure_code,
                    transaction_failure_message=charge.failure_message,
                    transaction_fraud_details=charge.fraud_details,
                    transaction_id=charge.id,
                    transaction_invoice=charge.invoice,
                    transaction_livemode=charge.livemode,
                    transaction_metadata=charge.metadata,
                    transaction_object=charge.object,
                    transaction_on_behalf_of=charge.on_behalf_of,
                    transaction_order=charge.order,
                    transaction_outcome=charge.outcome,
                    transaction_paid=charge.paid,
                    transaction_receipt_email=charge.receipt_email,
                    transaction_receipt_number=charge.receipt_number,
                    transaction_refunded=charge.refunded,
                    transaction_refunds=charge.refunds,
                    transaction_review=charge.review,
                    transaction_shipping=charge.shipping,
                    transaction_source=charge.source,
                    transaction_source_transfer=charge.source_transfer,
                    transaction_statement_descriptor=charge.statement_descriptor,
                    transaction_status=charge.status,
                    transaction_transfer_group=charge.transfer_group,
                )
                new_transaction.save()
            booking_item.save()
        except stripe.error.CardError as e:
            # Since it's a decline, stripe.error.CardError will be caught
            body = e.json_body
            err = body.get('error', {})
            instance.delete()
            messages.error(request, err.get('message'), extra_tags='card_error')
            return redirect('bookings:booking_landing')
        except stripe.error.RateLimitError as e:
            # Too many requests made to the API too quickly
            pass
        except stripe.error.InvalidRequestError as e:
            # Invalid parameters were supplied to Stripe's API
            pass
        except stripe.error.AuthenticationError as e:
            # Authentication with Stripe's API failed
            # (maybe you changed API keys recently)
            pass
        except stripe.error.APIConnectionError as e:
            # Network communication with Stripe failed
            pass
        except stripe.error.StripeError as e:
            # Display a very generic error to the user, and maybe send
            # yourself an email
            pass
        except Exception as e:
            # Something else happened, completely unrelated to Stripe
            pass

        email_template = get_template('e-mail/mail-booking-receipt.html').render()
        # email_template = email_template.replace("[[SpotimistLogo]]", "https://dev.spotimist.com/static/images/spotimist.png")
        email_template = email_template.replace("[[Firstname]]", instance.forename)
        email_template = email_template.replace("[[Lastname]]", instance.surname)
        email_template = email_template.replace("[[BookingID]]", booking_id)
        email_template = email_template.replace("[[qr]]", "https://{}{}".format(Site.objects.get_current().domain, qr_image))
        email_template = email_template.replace("[[BusinessImage]]", "https://{}{}".format(
            Site.objects.get_current().domain, activity_obj.business.get_image_url()))
        email_template = email_template.replace("[[ActivityName]]", activity_obj.name)
        email_template = email_template.replace("[[ServiceName]]", service_obj.name)
        email_template = email_template.replace("[[Date]]", date + " - " + time)
        email_template = email_template.replace("[[Price]]", str(service_price))
        try:
            u = unicode("€", "utf-8")
        except:
            u = "€"
        email_template = email_template.replace("[[Currency]]", u)

        email_subject = 'Booking Confirmation - {} {}'.format(activity_obj.business, service_obj.name)
        send_mail(
            email_subject,
            "",
            'Spotimist info@spotimist.com',
            [instance.email],
            fail_silently=False,
            auth_user=None,
            auth_password=None,
            connection=None,
            html_message=email_template
        )

    else:
        print("form not valid")
        print(form.errors.as_data())

    context = {
        "activity": activity_obj,
        "service": service_obj,
        "date": date,
        "time": time,
        "business": activity_obj.business,
        "qr_image": qr_image,
        "booking": instance,
        "guest": guest,
        "price": service_price

    }
    return render(request, "bookings/booking_confirmation.html", context)


def booking_confirmation(request):
    context = {
        "test": ""
    }

    return render(request, "bookings/booking_confirmation.html", context)


def transactions_history(request):
    import stripe

    transaction = TransactionHistory.objects.filter(
        transaction_business=Business.objects.get(user=request.user).id).order_by('-id')

    charge = request.POST.get('transaction_charge')
    if charge:
        if settings.STRIPE_LIVE_MODE:
            stripe.api_key = settings.STRIPE_LIVE_SECRET_KEY
        else:
            stripe.api_key = settings.STRIPE_TEST_SECRET_KEY
        refund = stripe.Refund.create(
            charge=charge
        )

        transaction_obj = TransactionHistory.objects.get(transaction_id=charge)
        transaction_obj.transaction_amount_refunded = refund.amount / 100
        transaction_obj.transaction_balance = refund.balance_transaction
        transaction_obj.transaction_id = refund.charge
        transaction_obj.transaction_currency = refund.currency
        transaction_obj.transaction_refund_id = refund.id
        transaction_obj.transaction_metadata = refund.metadata
        transaction_obj.transaction_object = refund.object
        transaction_obj.transaction_reason = refund.reason
        transaction_obj.transaction_receipt_number = refund.receipt_number
        transaction_obj.transaction_status = refund.status
        transaction_obj.transaction_withdraw = 0
        transaction_obj.save()

    total = 0
    for i in transaction:
        total += i.transaction_withdraw

    today = datetime.datetime.now().date()
    date_today = datetime.datetime.strptime(str(today), "%Y-%m-%d").strftime("%d %b %Y")
    context = {
        "transaction": transaction,
        "total": total,
        "date_today": date_today
    }

    return render(request, "bookings/dashboard/transactions_list.html", context)
