import datetime

from .models import Booking, BookingItem
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework import routers, serializers, viewsets, generics


class BookingItemSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = BookingItem
        fields = ['pk', 'name']

    def get_name(self, obj):
        booked_item = obj.get_booked_item()

        return booked_item


class BookingItemDetail(generics.RetrieveAPIView):
    serializer_class = BookingItemSerializer
    queryset = Booking.objects.all().prefetch_related('booking_items')


# Serializers define the API representation.
class BookingsSerializer(serializers.HyperlinkedModelSerializer):
    # booking_items = serializers.SerializerMethodField()
    booking_items = BookingItemSerializer(many=True, read_only=False)
    date = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField()
    message = serializers.SerializerMethodField()
    bookingStatus = serializers.SerializerMethodField()

    class Meta:
        model = Booking
        fields = [
            'message',
            'bookingStatus',
            'booking_id',
            # 'user', 
            'forename',
            'surname',
            'email',
            'date',
            'time',
            'total',
            'booking_items',
            # 'bookingitem_set'
        ]

    def get_date(self, obj):
        return obj.date_from.strftime('%d %B %Y')

    def get_time(self, obj):
        return obj.date_from.strftime('%H:%M')

    def get_message(self, obj):
        date_booked = obj.date_from.strftime('%d %B %Y')
        date_now = datetime.datetime.now().strftime('%d %B %Y')
        if date_booked == date_now:
            message = "Success"
        elif date_booked < date_now:
            message = "Error! This booking has expired on " + date_booked
        else:
            message = "Error! This booking will be valid on " + date_booked
        return message

    def get_bookingStatus(self, obj):
        date_booked = obj.date_from.strftime('%d %B %Y')
        date_now = datetime.datetime.now().strftime('%d %B %Y')
        if date_booked == date_now:
            bookingStatus = 1
        elif date_booked < date_now:
            bookingStatus = 0
        else:
            bookingStatus = 2
        return bookingStatus
    # def get_booking_items(self, obj):

    #     booking = obj
    #     booking_items = booking.get_booking_items()
    #     print(booking_items)
    #     return ""


# ViewSets define the view behavior.
class BookingsViewSet(viewsets.ModelViewSet):
    queryset = Booking.objects.all().prefetch_related('booking_items')
    serializer_class = BookingsSerializer
    # filter_fields = ('name')


class BookingList(generics.ListAPIView):
    serializer_class = BookingsSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        query_set = Booking.objects.all().prefetch_related('booking_items')

        return query_set


class BookingDetail(generics.RetrieveAPIView):
    multiple_lookup_fields = ("booking_id", "business_id")
    serializer_class = BookingsSerializer
    queryset = Booking.objects.all()

    def get_object(self):
        queryset = self.get_queryset()
        filter = {}
        for field in self.multiple_lookup_fields:
            filter[field] = self.kwargs[field]

        obj = get_object_or_404(queryset, **filter)
        return obj
