# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-05 07:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0015_transactionhistory_transaction_business'),
    ]

    operations = [
        migrations.AddField(
            model_name='transactionhistory',
            name='transaction_withdraw',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
    ]
