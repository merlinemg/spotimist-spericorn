# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-11 15:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0002_auto_20170911_1614'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bookingstatus',
            options={'verbose_name': 'Booking Status', 'verbose_name_plural': 'Booking Status'},
        ),
    ]
