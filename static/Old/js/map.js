jQuery(function ($) {
    'use strict';
    initialize();

    function initialize() {


        var mapOptions = {
            center: new google.maps.LatLng(53.3498053, -6.2603097),
            zoom: 14,
            scrollwheel: false
        };
        var map = new google.maps.Map(document.getElementById('map'),
                mapOptions);
        
        setMarkers(map, locations)
        
        var input = /** @type {HTMLInputElement} */(
                document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            draggable: true,
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        google.maps.event.addListener(marker, "mouseup", function (event) {
            $('#input-latitude').val(this.position.lat());
            $('#input-longitude').val(this.position.lng());
        });

        
    }
    
    function setMarkers(map, locations) {

            var marker, i

            for (i = 0; i < locations.length; i++)
            {

                var loan = locations[i][0]
                var lat = locations[i][1]
                var long = locations[i][2]
                var add = locations[i][3]                
                var id = locations[i][4]

                var latlngset = new google.maps.LatLng(lat, long);

                var marker = new google.maps.Marker({
                    map: map, title: loan, position: latlngset
                });
                map.setCenter(marker.getPosition())


                var content =  "<a href=\"" + activityUrl + id +"\">" + add + "</a>";

                var infowindow = new google.maps.InfoWindow()

                google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                    return function () {
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    };
                })(marker, content, infowindow));

            }
        }

    if ($('#map-canvas').length != 0) {
        google.maps.event.addDomListener(window, 'load', initialize);
    }
});
