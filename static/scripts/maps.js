// Default infoBox Rating Type
var infoBox_ratingType = 'star-rating';
var overlays = [];

(function($){
    "use strict";
    function mainMap() {

      // Locations
      // ----------------------------------------------- //
      var ib = new InfoBox();

      // Infobox Output
      function locationData(locationURL,locationImg,locationTitle, locationAddress, locationRating, locationRatingCounter) {

          return(''+
            '<a href="'+ locationURL +'" class="listing-img-container">'+
               '<div class="infoBox-close"><i class="fa fa-times"></i></div>'+
               '<img src="'+locationImg+'" alt="">'+

               '<div class="listing-item-content">'+
                  '<h3>'+locationTitle+'</h3>'+
                  '<span>'+locationAddress+'</span>'+
               '</div>'+

            '</a>'+

            '<div class="listing-content">'+
               '<div class="listing-title">'+
                  '<div class="'+infoBox_ratingType+'" data-rating="'+locationRating+'"><div class="rating-counter">('+locationRatingCounter+' reviews)</div></div>'+
               '</div>'+
            '</div>')
      }

      // Chosen Rating Type
      google.maps.event.addListener(ib,'domready',function(){
         if (infoBox_ratingType = 'numerical-rating') {
            numericalRating('.infoBox .'+infoBox_ratingType+'');
         }
         if (infoBox_ratingType = 'star-rating') {
            starRating('.infoBox .'+infoBox_ratingType+'');
         }
      });




      // Map Attributes
      // ----------------------------------------------- //

      var mapZoomAttr = $('#map').attr('data-map-zoom');
      var mapScrollAttr = $('#map').attr('data-map-scroll');

      if (typeof mapZoomAttr !== typeof undefined && mapZoomAttr !== false) {
          var zoomLevel = parseInt(mapZoomAttr);
      } else {
          var zoomLevel = 5;
      }

      if (typeof mapScrollAttr !== typeof undefined && mapScrollAttr !== false) {
         var scrollEnabled = parseInt(mapScrollAttr);
      } else {
        var scrollEnabled = false;
      }

      // Main Map
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoomLevel,
        scrollwheel: scrollEnabled,
        center: new google.maps.LatLng(53.3498053, -6.2603097),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        clickableIcons: false,
        gestureHandling: 'cooperative',

        // Google Map Style
        styles: [{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"23"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#f38eb0"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#ced7db"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#ffa5a8"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#c7e5c8"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"color":"#d6cbc7"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#c4c9e8"}]},{"featureType":"poi.sports_complex","elementType":"geometry.fill","stylers":[{"color":"#b1eaf1"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":"100"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd4a5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffe9d2"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"weight":"3.00"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"weight":"0.30"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"36"}]},{"featureType":"road.local","elementType":"labels.text.stroke","stylers":[{"color":"#e9e5dc"},{"lightness":"30"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#d2e7f7"}]}]

      });


      var input = document.getElementById('autocomplete');

      // single_map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

      var autocomplete = new google.maps.places.Autocomplete(input);

      // Bind the map's bounds (viewport) property to the autocomplete object,
      // so that the autocomplete requests use the current map bounds for the
      // bounds option in the request.
      autocomplete.bindTo('bounds', map);

      google.maps.event.addListener(autocomplete, 'place_changed', function () {

        // Before adding a marker through autocompletion we clear all existing ones on the map
        for (var i = 0; i < overlays.length; i++) {
          overlays[i].setMap(null);
        }

        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {

          map.fitBounds(place.geometry.viewport);
          map.setZoom(15);
        } else {
          map.setCenter(place.geometry.location);

          map.setZoom(15);
        }
      });


      // Marker highlighting when hovering listing item
      $('.listing-item-container').on('mouseover', function(){

        var listingAttr = $(this).data('marker-id');

        if(listingAttr !== undefined) {
          var listing_id = $(this).data('marker-id') - 1;
          var marker_div = allMarkers[listing_id].div;

          $(marker_div).addClass('clicked');

          $(this).on('mouseout', function(){
              if ($(marker_div).is(":not(.infoBox-opened)")) {
                 $(marker_div).removeClass('clicked');
              }
           });
        }

      });


      // Infobox
      // ----------------------------------------------- //

      var boxText = document.createElement("div");
      boxText.className = 'map-box'

      var currentInfobox;

      var boxOptions = {
              content: boxText,
              disableAutoPan: false,
              alignBottom : true,
              maxWidth: 0,
              pixelOffset: new google.maps.Size(-134, -55),
              zIndex: null,
              boxStyle: {
                width: "270px"
              },
              closeBoxMargin: "0",
              closeBoxURL: "",
              infoBoxClearance: new google.maps.Size(25, 25),
              isHidden: false,
              pane: "floatPane",
              enableEventPropagation: false,
      };


      var markerCluster, overlay, i;
      var allMarkers = [];

      var clusterStyles = [
        {
          textColor: 'white',
          url: '',
          height: 50,
          width: 50
        }
      ];


      var markerIco;

      // Locations
//      var locations = [];
//      var business_api_url = $('#map').attr('data-api-url');
//      $.getJSON( business_api_url, function( data ) {
//        var id = 1;
//        $.each( data, function( key, val ) {
//          var business = [ locationData(val.absolute_url, val.image, val.name, '964 School Street, New York', '3.5', '12'), parseFloat(val.latitude), parseFloat(val.longitude), id, '<i class="im im-icon-Map-Marker2"></i>']
//          locations.push(business);
//          id++;
//        });
//      }).done(function(data){
//          for (i = 0; i < locations.length; i++) {
//
//              markerIco = locations[i][4];
//
//              var overlaypositions = new google.maps.LatLng(locations[i][1], locations[i][2]),
//
//              overlay = new CustomMarker(
//                overlaypositions,
//                map,
//                {
//                  marker_id: i
//                },
//                markerIco
//              );
//
//              allMarkers.push(overlay);
//
//              google.maps.event.addDomListener(overlay, 'click', (function(overlay, i) {
//
//                return function() {
//                    ib.setOptions(boxOptions);
//                    boxText.innerHTML = locations[i][0];
//                    ib.open(map, overlay);
//
//                    currentInfobox = locations[i][3];
//                    // var latLng = new google.maps.LatLng(locations[i][1], locations[i][2]);
//                    // map.panTo(latLng);
//                    // map.panBy(0,-90);
//
//
//                  google.maps.event.addListener(ib,'domready',function(){
//                    $('.infoBox-close').click(function(e) {
//                        e.preventDefault();
//                        ib.close();
//                        $('.map-marker-container').removeClass('clicked infoBox-opened');
//                    });
//
//                  });
//
//                }
//              })(overlay, i));
//
//            }
//
//            // Marker Clusterer Init
//            // ----------------------------------------------- //
//
//            var options = {
//              imagePath: 'images/',
//              styles : clusterStyles,
//              minClusterSize : 2
//          };
//          markerCluster = new MarkerClusterer(map, allMarkers, options);
//
//          google.maps.event.addDomListener(window, "resize", function() {
//            var center = map.getCenter();
//              google.maps.event.trigger(map, "resize");
//              map.setCenter(center);
//          });
//        });

      // Custom User Interface Elements
      // ----------------------------------------------- //

      // Custom Zoom-In and Zoom-Out Buttons
        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);

        function ZoomControl(controlDiv, map) {

          zoomControlDiv.index = 1;
          map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
          // Creating divs & styles for custom zoom control
          controlDiv.style.padding = '5px';
          controlDiv.className = "zoomControlWrapper";

          // Set CSS for the control wrapper
          var controlWrapper = document.createElement('div');
          controlDiv.appendChild(controlWrapper);

          // Set CSS for the zoomIn
          var zoomInButton = document.createElement('div');
          zoomInButton.className = "custom-zoom-in";
          controlWrapper.appendChild(zoomInButton);

          // Set CSS for the zoomOut
          var zoomOutButton = document.createElement('div');
          zoomOutButton.className = "custom-zoom-out";
          controlWrapper.appendChild(zoomOutButton);

          // Setup the click event listener - zoomIn
          google.maps.event.addDomListener(zoomInButton, 'click', function() {
            map.setZoom(map.getZoom() + 1);
          });

          // Setup the click event listener - zoomOut
          google.maps.event.addDomListener(zoomOutButton, 'click', function() {
            map.setZoom(map.getZoom() - 1);
          });

      }


      // Scroll enabling button
      var scrollEnabling = $('#scrollEnabling');

      $(scrollEnabling).click(function(e){
          e.preventDefault();
          $(this).toggleClass("enabled");

          if ( $(this).is(".enabled") ) {
             map.setOptions({'scrollwheel': true});
          } else {
             map.setOptions({'scrollwheel': false});
          }
      })


//      // Geo Location Button
//      $(".input-with-icon.location a").click(function(e){
//          e.preventDefault();
//          geolocate();
//      });

      $(document).ready(function(){
        geolocate();
        localStorage['ajax_key'] = 'location_key';
      });

      function geolocate() {
          if (navigator.geolocation) {
              if(localStorage['c_lat'] && localStorage['c_lng']){
                var lat = localStorage['c_lat'];
                var lng = localStorage['c_lng'];
                var pos = new google.maps.LatLng(lat, lng);
                $('#curr_lat').val(lat);
                $('#curr_lng').val(lng);
                currentLocation();
                map.setCenter(pos);
                map.setZoom(12);
              }
              else{
                navigator.geolocation.getCurrentPosition(showPosition, showError);
              }

          }

      }

      function showPosition(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            var pos = new google.maps.LatLng(lat, lng);
            localStorage['c_lat'] = lat;
            localStorage['c_lng'] = lng;
            $('#curr_lat').val(lat);
            $('#curr_lng').val(lng);
            currentLocation();
            map.setCenter(pos);
            map.setZoom(12);
            localStorage['ajax_key'] = 'location_key';
        }

      function showError(error) {
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    map.innerHTML = "User denied the request for Geolocation."
                    noLocation();
                    break;
                case error.POSITION_UNAVAILABLE:
                    map.innerHTML = "Location information is unavailable."
                    noLocation();
                    break;
                case error.TIMEOUT:
                    map.innerHTML = "The request to get user location timed out."
                    noLocation();
                    break;
                case error.UNKNOWN_ERROR:
                    map.innerHTML = "An unknown error occurred."
                    noLocation();
                    break;
            }
        }
    }




    // Map Init
    var map =  document.getElementById('map');
    if (typeof(map) != 'undefined' && map != null) {
      google.maps.event.addDomListener(window, 'load',  mainMap);
      // google.maps.event.addDomListener(window, 'resize',  mainMap);
    }


    // ---------------- Main Map / End ---------------- //


    // Single Listing Map
    // ----------------------------------------------- //

    function singleListingMap() {
      var myLatlng = new google.maps.LatLng({lng: $( '#singleListingMap' ).data('longitude'),lat: $( '#singleListingMap' ).data('latitude'), });

      var single_map = new google.maps.Map(document.getElementById('singleListingMap'), {
        zoom: 15,
        center: myLatlng,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        clickableIcons: false,
        styles:  [{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"23"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#f38eb0"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#ced7db"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#ffa5a8"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#c7e5c8"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"color":"#d6cbc7"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#c4c9e8"}]},{"featureType":"poi.sports_complex","elementType":"geometry.fill","stylers":[{"color":"#b1eaf1"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":"100"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd4a5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffe9d2"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"weight":"3.00"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"weight":"0.30"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"36"}]},{"featureType":"road.local","elementType":"labels.text.stroke","stylers":[{"color":"#e9e5dc"},{"lightness":"30"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#d2e7f7"}]}]
      });

      // Steet View Button
      $('#streetView').click(function(e){
         e.preventDefault();
         single_map.getStreetView().setOptions({visible:true,position:myLatlng});
         // $(this).css('display', 'none')
      });


      // Custom zoom buttons
      var zoomControlDiv = document.createElement('div');
      var zoomControl = new ZoomControl(zoomControlDiv, single_map);

      function ZoomControl(controlDiv, single_map) {

        zoomControlDiv.index = 1;
        single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);

        controlDiv.style.padding = '5px';

        var controlWrapper = document.createElement('div');
        controlDiv.appendChild(controlWrapper);

        var zoomInButton = document.createElement('div');
        zoomInButton.className = "custom-zoom-in";
        controlWrapper.appendChild(zoomInButton);

        var zoomOutButton = document.createElement('div');
        zoomOutButton.className = "custom-zoom-out";
        controlWrapper.appendChild(zoomOutButton);

        google.maps.event.addDomListener(zoomInButton, 'click', function() {
          single_map.setZoom(single_map.getZoom() + 1);
        });

        google.maps.event.addDomListener(zoomOutButton, 'click', function() {
          single_map.setZoom(single_map.getZoom() - 1);
        });

      }


      // Marker
      var singleMapIco =  "<i class='"+$('#singleListingMap').data('map-icon')+"'></i>";

      new CustomMarker(
        myLatlng,
        single_map,
        {
          marker_id: '1'
        },
        singleMapIco
      );


    }

    // Single Listing Map Init
    var single_map =  document.getElementById('singleListingMap');
    if (typeof(single_map) != 'undefined' && single_map != null) {
      google.maps.event.addDomListener(window, 'load',  singleListingMap);
      // google.maps.event.addDomListener(window, 'resize',  singleListingMap);
    }

    // -------------- Single Listing Map / End -------------- //

    // Custom Map Marker
    // ----------------------------------------------- //


    function CustomMarker(latlng, map, args, markerIco) {
      this.latlng = latlng;
      this.args = args;
      this.markerIco = markerIco;
      this.setMap(map);
    }
    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function() {

      var self = this;

      var div = this.div;

      if (!div) {
        div = this.div = document.createElement('div');
        div.className = 'map-marker-container';

        div.innerHTML = '<div class="marker-container">'+
                            '<div class="marker-card">'+
                               '<div class="front face">' + self.markerIco + '</div>'+
                               '<div class="back face">' + self.markerIco + '</div>'+
                               '<div class="marker-arrow"></div>'+
                            '</div>'+
                          '</div>'


        // Clicked marker highlight
        google.maps.event.addDomListener(div, "click", function(event) {
            $('.map-marker-container').removeClass('clicked infoBox-opened');
            $('.map-marker-container').not($(this)).removeAttr('data-attr-clicked');
            if (!$(this).attr('data-attr-clicked')){
                google.maps.event.trigger(self, "click");
                $(this).addClass('clicked infoBox-opened');
                $(this).attr('data-attr-clicked','1');
                starRating('.infoBox .star-rating');
                googleEvent();
            }
            else {
                $(".infoBox-close").trigger("click");
                $(this).removeAttr('data-attr-clicked');
            }
        });

        if (typeof(self.args.marker_id) !== 'undefined') {
          div.dataset.marker_id = self.args.marker_id;
        }

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);

        function googleEvent(){
            google.maps.event.trigger(self, "click");
            $('.infoBox-close').click(function(e) {
                e.preventDefault();
                $('.infoBox').hide();
                $('.map-marker-container').removeClass('clicked infoBox-opened');
            });
        }

      }

      var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

      if (point) {
        div.style.left = (point.x) + 'px';
        div.style.top = (point.y) + 'px';
      }
    };

    CustomMarker.prototype.remove = function() {
      if (this.div) {
        this.div.parentNode.removeChild(this.div);
        this.div = null; $(this).removeClass('clicked');
      }
    };

    CustomMarker.prototype.getPosition = function() { return this.latlng; };

    // -------------- Custom Map Marker / End -------------- //



    function singleListingMapEdit() {
      var current_lat = $( '#singleListingMap' ).data('latitude');
      var current_lng = $( '#singleListingMap' ).data('longitude');

      var myLatlng = new google.maps.LatLng({lng: current_lng, lat: current_lat});

      var single_map = new google.maps.Map(document.getElementById('singleListingMap'), {
        zoom: 15,
        center: myLatlng,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        clickableIcons: false,
        styles:  [{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"23"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#f38eb0"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#ced7db"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#ffa5a8"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#c7e5c8"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"color":"#d6cbc7"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#c4c9e8"}]},{"featureType":"poi.sports_complex","elementType":"geometry.fill","stylers":[{"color":"#b1eaf1"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":"100"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd4a5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffe9d2"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"weight":"3.00"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"weight":"0.30"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"36"}]},{"featureType":"road.local","elementType":"labels.text.stroke","stylers":[{"color":"#e9e5dc"},{"lightness":"30"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#d2e7f7"}]}]
      });

      if (current_lat == null || current_lng == null)
      {
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            single_map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, single_map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, single_map.getCenter());
        }
      }

      var input = document.getElementById('autocomplete');

      // single_map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

      var autocomplete = new google.maps.places.Autocomplete(input);

      // Bind the map's bounds (viewport) property to the autocomplete object,
      // so that the autocomplete requests use the current map bounds for the
      // bounds option in the request.
      autocomplete.bindTo('bounds', single_map);

      google.maps.event.addListener(autocomplete, 'place_changed', function () {

        // Before adding a marker through autocompletion we clear all existing ones on the map
        for (var i = 0; i < overlays.length; i++) {
          overlays[i].setMap(null);
        }

        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
          single_map.fitBounds(place.geometry.viewport);
          single_map.setZoom(15);
        } else {
          single_map.setCenter(place.geometry.location);
          single_map.setZoom(15);
        }

        var latLng = place.geometry.location;

        var singleMapIco =  "<i class='"+$('#singleListingMap').data('map-icon')+"'></i>";
        overlays.push( new DraggableOverlay(single_map,
          latLng,
          singleMapIco
                ));

        var latitude = document.getElementById('id_latitude');
        var longitude = document.getElementById('id_longitude');
        var address = document.getElementById('id_address');
        var postCode = document.getElementById('id_postCode');
        var city = document.getElementById('id_city');
        var country = document.getElementById('id_country');

        latitude.value = latLng.lat();
        longitude.value = latLng.lng();

        // GMap address component types
        var street_number = "";
        var route = "";
        var locality = "";
        var postal_town = "";
        var post_code = "";
        var countryGmap = "";

        if (place.address_components) {

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                switch (addressType){
                    case "street_number": street_number = place.address_components[i]["long_name"];
                        break;
                    case "route": route = place.address_components[i]["long_name"];
                        break;
                    case "locality": locality = place.address_components[i]["long_name"];
                        break;
                    case "postal_town": postal_town = place.address_components[i]["long_name"];
                        break;
                    case "postal_code": post_code = place.address_components[i]["long_name"];
                        break;
                    case "country": countryGmap = place.address_components[i]["long_name"];
                        break;
                }
            }

            address.value = [street_number, route].join(' ');
            postCode.value = post_code;
            if (locality != "")
              city.value = locality;
            else
              city.value = postal_town;
            country.value = countryGmap;
        }
      });

      function fillInAddress() {

          // Get each component of the address from the place details
          // and fill the corresponding field on the form.
          for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
              }
          }
      }

      // Custom zoom buttons
        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, single_map);

        function ZoomControl(controlDiv, single_map) {

          zoomControlDiv.index = 1;
          single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);

          controlDiv.style.padding = '5px';

          var controlWrapper = document.createElement('div');
          controlDiv.appendChild(controlWrapper);

          var zoomInButton = document.createElement('div');
          zoomInButton.className = "custom-zoom-in";
          controlWrapper.appendChild(zoomInButton);

          var zoomOutButton = document.createElement('div');
          zoomOutButton.className = "custom-zoom-out";
          controlWrapper.appendChild(zoomOutButton);

          google.maps.event.addDomListener(zoomInButton, 'click', function() {
            single_map.setZoom(single_map.getZoom() + 1);
          });

          google.maps.event.addDomListener(zoomOutButton, 'click', function() {
            single_map.setZoom(single_map.getZoom() - 1);
          });

        }

        var singleMapIco =  "<i class='"+$('#singleListingMap').data('map-icon')+"'></i>";
        overlays.push( new DraggableOverlay(single_map,
          single_map.getCenter(),
          singleMapIco
                ));

    }

    DraggableOverlay.prototype = new google.maps.OverlayView();

    DraggableOverlay.prototype.onAdd = function() {
      var container=document.createElement('div'),
          that=this;

      if(typeof this.get('content').nodeName!=='undefined'){
        container.appendChild(this.get('content'));
      }
      else{
        if(typeof this.get('content')==='string'){
          container.className = 'map-marker-container';
          container.innerHTML='<div class="marker-container" style="cursor:move;">'+
                                  '<div class="marker-card">'+
                                  '<div class="front face">' + this.get('content') + '</div>'+
                                  '<div class="back face">' + this.get('content') + '</div>'+
                                  '<div class="marker-arrow"></div>'+
                                  '</div>'+
                              '</div>';
        }
        else{
          return;
        }
      }
      container.style.position='absolute';
      container.draggable=true;
          google.maps.event.addDomListener(this.get('map').getDiv(),
                                           'mouseleave',
                                            function(){
              google.maps.event.trigger(container,'mouseup');
            }
          );

          google.maps.event.addDomListener(container,'mousedown', function(e){
            this.style.cursor='move';
            that.map.set('draggable',false);
            that.set('origin',e);

            that.moveHandler  = google.maps.event.addDomListener(that.get('map').getDiv(),
                                                                 'mousemove',
                                                                 function(e){
              var origin = that.get('origin'),
                  left   = origin.clientX-e.clientX,
                  top    = origin.clientY-e.clientY,
                  pos    = that.getProjection()
                            .fromLatLngToDivPixel(that.get('position')),
                  latLng = that.getProjection()
                            .fromDivPixelToLatLng(new google.maps.Point(pos.x-left,
                                                                        pos.y-top));
                  that.set('origin',e);
                  that.set('position',latLng);
                  that.draw();


              });


            }
         );

          google.maps.event.addDomListener(container,'mouseup',function(){
              if (that.getProjection() != null)
              {
                var pos = that.getProjection().fromLatLngToDivPixel(that.get('position'));
                var latLng = that.getProjection().fromDivPixelToLatLng(new google.maps.Point(pos.x, pos.y));
                var latitude = document.getElementById('id_latitude');
                var longitude = document.getElementById('id_longitude');
                latitude.value = latLng.lat();
                longitude.value = latLng.lng();

                // var testContainerPos = that.getProjection().fromContainerPixelToLatLng(new google.maps.Point(pos.x, pos.y));

                that.map.set('draggable',true);
                this.style.cursor='default';
                google.maps.event.removeListener(that.moveHandler);
              }

          });


      this.set('container',container)
      this.getPanes().floatPane.appendChild(container);
    };

    function DraggableOverlay(map,position,content){
      if(typeof draw==='function'){
        this.draw=draw;
      }
      this.setValues({
                     position:position,
                     container:null,
                     content:content,
                     map:map
                     });
    }

    DraggableOverlay.prototype.draw = function() {
      var pos = this.getProjection().fromLatLngToDivPixel(this.get('position'));
      this.get('container').style.left = pos.x + 'px';
      this.get('container').style.top = pos.y + 'px';
    };

    DraggableOverlay.prototype.onRemove = function() {
      this.get('container').parentNode.removeChild(this.get('container'));
      this.set('container',null)
    };
    // Single Listing Map Init
    var single_map =  $('#singleListingMap').data('map-type');
    if (typeof(single_map) != 'undefined' && single_map != null) {
      google.maps.event.addDomListener(window, 'load',  singleListingMapEdit);
      // google.maps.event.addDomListener(window, 'resize',  singleListingMapEdit);
    }



})(this.jQuery);