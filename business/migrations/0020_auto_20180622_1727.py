# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-22 17:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0019_auto_20180530_1222'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='business',
            options={'ordering': ['-id'], 'verbose_name': 'Business', 'verbose_name_plural': 'Businesses'},
        ),
    ]
