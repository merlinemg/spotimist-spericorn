# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-18 17:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0021_merge_20180625_0442'),
    ]

    operations = [
        migrations.AddField(
            model_name='business',
            name='booking_fee',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
