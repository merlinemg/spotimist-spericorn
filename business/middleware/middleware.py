from django.core.cache import cache
from django.utils.deprecation import MiddlewareMixin
from spotimist import settings
from geoip import geolite2


class BussinessMiddleware(MiddlewareMixin):

    def process_request(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        try:
            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[-1].strip()
                loc_ip = geolite2.lookup(ip)
                settings.TIME_ZONE = loc_ip.timezone
        except:
            ip = request.META.get('REMOTE_ADDR')
            loc_ip = geolite2.lookup(ip)
            settings.TIME_ZONE = loc_ip.timezone
