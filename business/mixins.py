import geopy
import geopy.distance
from categories.models import Category
from django.db.models import Q
from activities.models import Activity
from business.models import Business
from services.models import Service, RecurringService
import datetime


class ViewMixins(object):

    def business_filter(self, ajax_key, search, location, categories, dist, latitude, longitude, curr_lat, curr_lng, ):
        query_set = ''
        empty_array = []
        temp_empty_array = []
        location_array = []
        temp_location_array = []
        search_array = []
        temp_search_array = []
        search_location_array = []
        temp_search_location_array = []
        full_array = []
        temp_full_array = []
        category_array = []
        temp_category_array = []
        location_category_array = []
        temp_location_category_array = []
        search_category_array = []
        temp_search_category_array = []
        curr_array = []
        curr_empty_array = []
        current_date = datetime.datetime.now().date()
        no_array = []
        temp_no_array = []
        if not categories:
            categories = []
        if ajax_key == 'location_key':
            cat_array = Category.objects.filter(name__icontains=categories)
            all_query = Business.objects.all()
            filtered_query = self.home_filter(cat_array, search, location, curr_lat, curr_lng)

            if filtered_query:
                all_query = filtered_query
            if not filtered_query:
                if cat_array or search or location:
                    all_query = []
            for bsns in all_query:
                get_activity = Activity.objects.filter(business=bsns)
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    pt1 = geopy.Point(bsns.latitude, bsns.longitude)
                                    pt2 = geopy.Point(curr_lat, curr_lng)
                                    distance = geopy.distance.distance(pt1, pt2).km
                                    distance = round(distance)
                                    if int(distance) <= 50 and bsns not in temp_empty_array:
                                        empty_array.append(bsns)
                                        temp_empty_array.append(bsns)
            query_set = empty_array

        elif ajax_key == 'nolocation_key':
            for busins in Business.objects.filter(addressGmap__icontains='Dublin'):
                get_activity = Activity.objects.filter(business=busins)
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    if busins not in temp_no_array:
                                        no_array.append(busins)
                                        temp_no_array.append(busins)
            query_set = no_array

        if ajax_key == 'filter_key':

            if search == '' and location != '' and categories == []:
                for buss in Business.objects.all():
                    get_activity = Activity.objects.filter(business=buss)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(buss.latitude, buss.longitude)
                                        pt2 = geopy.Point(latitude, longitude)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= int(dist) and buss not in temp_location_array:
                                            location_array.append(buss)
                                            temp_location_array.append(buss)
                query_set = location_array

            elif search != '' and location == '' and categories == []:
                for busines in Business.objects.all():
                    get_activity = Activity.objects.filter(
                        Q(name__icontains=search) | Q(business__name__icontains=search), business=busines)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(busines.latitude, busines.longitude)
                                        pt2 = geopy.Point(curr_lat, curr_lng)
                                        distance = geopy.distance.distance(pt1, pt2).km

                                        distance = round(distance)
                                        if int(distance) <= int(dist) and busines not in temp_search_array:
                                            search_array.append(busines)
                                            temp_search_array.append(busines)
                query_set = search_array

            elif search != '' and location != '' and categories == []:
                for busines in Business.objects.all():
                    get_activity = Activity.objects.filter(
                        Q(name__icontains=search) | Q(business__name__icontains=search), Q(business=busines))
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(busines.latitude, busines.longitude)
                                        pt2 = geopy.Point(latitude, longitude)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= int(dist) and busines not in temp_search_location_array:
                                            search_location_array.append(busines)
                                            temp_search_location_array.append(busines)
                query_set = search_location_array

            elif search != '' and location != '' and categories != []:
                for category in categories:
                    for business in Business.objects.filter(categories__name=category):
                        get_activity = Activity.objects.filter(
                            Q(name__icontains=search) | Q(business__name__icontains=search),
                            business=business)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(business.latitude, business.longitude)
                                            pt2 = geopy.Point(latitude, longitude)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(dist) and business not in temp_full_array:
                                                full_array.append(business)
                                                temp_full_array.append(business)
                query_set = full_array

            elif search == '' and location == '' and categories != []:
                for category in categories:
                    for busnss in Business.objects.filter(categories__name=category):
                        get_activity = Activity.objects.filter(business=busnss)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(busnss.latitude, busnss.longitude)
                                            pt2 = geopy.Point(curr_lat, curr_lng)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(dist) and busnss not in temp_category_array:
                                                category_array.append(busnss)
                                                temp_category_array.append(busnss)
                query_set = category_array

            elif search == '' and location != '' and categories != []:
                for category in categories:
                    for busnss in Business.objects.filter(categories__name=category):
                        get_activity = Activity.objects.filter(business=busnss)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(busnss.latitude, busnss.longitude)
                                            pt2 = geopy.Point(latitude, longitude)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(dist) and busnss not in temp_location_category_array:
                                                location_category_array.append(busnss)
                                                temp_location_category_array.append(busnss)
                query_set = location_category_array

            elif search != '' and location == '' and categories != []:
                for category in categories:
                    for srch_busnss in Business.objects.filter(categories__name=category):
                        get_activity = Activity.objects.filter(
                            Q(name__icontains=search) | Q(business__name__icontains=search),
                            business=srch_busnss)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(srch_busnss.latitude, srch_busnss.longitude)
                                            pt2 = geopy.Point(curr_lat, curr_lng)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(dist) and srch_busnss not in temp_search_category_array:
                                                search_category_array.append(srch_busnss)
                                                temp_search_category_array.append(srch_busnss)
                query_set = search_category_array

            elif search == '' and location == '' and categories == []:
                for bsns in Business.objects.all():
                    get_activity = Activity.objects.filter(business=bsns)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(bsns.latitude, bsns.longitude)
                                        pt2 = geopy.Point(curr_lat, curr_lng)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= 100 and bsns not in curr_empty_array:
                                            curr_array.append(bsns)
                                            curr_empty_array.append(bsns)
                query_set = curr_array

        return query_set

    def home_filter(self, cat_object, dat_query, loc_query, home_lat=None, home_lng=None):
        query_set = ''
        cat_array = []
        src_array = []
        temp_src_array = []
        loc_src_array = []
        src_cat_array = []
        temp_src_cat_array = []
        temp_loc_src_array = []
        loc_cat_array = []
        temp_loc_cat_array = []
        full_array = []
        temp_full_array = []
        location_array = []
        temp_location_array = []
        category_array = []
        temp_category_array = []
        current_date = datetime.datetime.now().date()
        for item in cat_object:
            cat_array.append(item.id)

        if dat_query == '' and loc_query == '' and cat_object.exists() is True:

            # query_set = Business.objects.filter(categories__id__in=cat_array)
            for category in cat_object:
                for busnss in Business.objects.filter(categories__name=category):
                    get_activity = Activity.objects.filter(business=busnss)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(busnss.latitude, busnss.longitude)
                                        pt2 = geopy.Point(home_lat, home_lng)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= 100 and busnss not in temp_category_array:
                                            category_array.append(busnss)
                                            temp_category_array.append(busnss)
            query_set = category_array

        elif dat_query == '' and loc_query != '' and cat_object.exists() is False:
            for buss in Business.objects.all():
                get_activity = Activity.objects.filter(business=buss)
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    pt1 = geopy.Point(buss.latitude, buss.longitude)
                                    pt2 = geopy.Point(home_lat, home_lng)
                                    distance = geopy.distance.distance(pt1, pt2).km
                                    distance = round(distance)
                                    if int(distance) <= 100 and buss not in temp_location_array:
                                        location_array.append(buss)
                                        temp_location_array.append(buss)
            query_set = location_array

        elif dat_query != '' and loc_query == '' and cat_object.exists() is False:
            for busines in Business.objects.all():
                get_activity = Activity.objects.filter(
                    Q(name__icontains=dat_query) | Q(business__name__icontains=dat_query),
                    business=busines)
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    pt1 = geopy.Point(busines.latitude, busines.longitude)
                                    pt2 = geopy.Point(home_lat, home_lng)
                                    distance = geopy.distance.distance(pt1, pt2).km

                                    distance = round(distance)
                                    if int(distance) <= 100 and busines not in temp_src_array:
                                        src_array.append(busines)
                                        temp_src_array.append(busines)
            query_set = src_array

        elif dat_query != '' and loc_query != '' and cat_object.exists() is False:

            for busines in Business.objects.all():
                get_activity = Activity.objects.filter(
                    Q(name__icontains=dat_query) | Q(business__name__icontains=dat_query),
                    Q(business=busines))
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    pt1 = geopy.Point(busines.latitude, busines.longitude)
                                    pt2 = geopy.Point(home_lat, home_lng)
                                    distance = geopy.distance.distance(pt1, pt2).km
                                    distance = round(distance)
                                    if int(distance) <= 100 and busines not in temp_loc_src_array:
                                        loc_src_array.append(busines)
                                        temp_loc_src_array.append(busines)
            query_set = loc_src_array

        elif dat_query != '' and loc_query == '' and cat_object.exists() is True:
            for category in cat_object:
                for srch_busnss in Business.objects.filter(categories__name=category):
                    get_activity = Activity.objects.filter(
                        Q(name__icontains=dat_query) | Q(business__name__icontains=dat_query),
                        business=srch_busnss)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(srch_busnss.latitude, srch_busnss.longitude)
                                        pt2 = geopy.Point(home_lat, home_lng)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= 100 and srch_busnss not in temp_src_cat_array:
                                            src_cat_array.append(srch_busnss)
                                            temp_src_cat_array.append(srch_busnss)
            query_set = src_cat_array

        elif dat_query == '' and loc_query != '' and cat_object.exists() is True:
            for category in cat_object:
                for busnss in Business.objects.filter(categories__name=category):
                    get_activity = Activity.objects.filter(business=busnss)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(busnss.latitude, busnss.longitude)
                                        pt2 = geopy.Point(home_lat, home_lng)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= 100 and busnss not in temp_loc_cat_array:
                                            loc_cat_array.append(busnss)
                                            temp_loc_cat_array.append(busnss)
            query_set = loc_cat_array

        elif dat_query != '' and loc_query != '' and cat_object.exists() is True:

            for category in cat_object:
                for business in Business.objects.filter(categories__name=category):
                    get_activity = Activity.objects.filter(
                        Q(name__icontains=dat_query) | Q(business__name__icontains=dat_query),
                        business=business)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(business.latitude, business.longitude)
                                        pt2 = geopy.Point(home_lat, home_lng)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= 100 and business not in temp_full_array:
                                            full_array.append(business)
                                            temp_full_array.append(business)
            query_set = full_array

        return query_set
