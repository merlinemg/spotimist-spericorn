from django.contrib import admin

# Register your models here.
from .models import Business, BusinessImage, OpeningHours

class PropertyImageInLine(admin.TabularInline):
    model = BusinessImage
    extra = 3

class PropertyOpeningHoursInLine(admin.TabularInline):
    model = OpeningHours
    
# class PropertyAdmin(admin.ModelAdmin):
#     inlines = [PropertyImageInLine,]

class BusinessModelAdmin(admin.ModelAdmin):
    list_display = ["name", "created"]
    list_filter = ["name", "is_active"]
    search_fields = ["name", "description"]
    inlines = [PropertyOpeningHoursInLine, PropertyImageInLine]
    class Meta:
        model = Business

admin.site.register(Business, BusinessModelAdmin)
# admin.site.register(BusinessImage, PropertyImageInLine)