import geopy
from django.db.models import Q

from activities.models import Activity
from business.mixins import ViewMixins
from services.models import RecurringService
from .models import Business, OpeningHours
from categories.models import Category
from rest_framework import routers, serializers, viewsets, generics
from activities.seralizers import ActivitySerializer
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = [
            'id',
            'url',
            'name',
            'slug'
        ]


# ViewSets define the view behavior.
class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class OpeningHoursSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = OpeningHours
        fields = [
            'weekday',
            'from_hour',
            'to_hour'
        ]


# Serializers define the API representation.
class BusinessSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    # categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    categories = CategorySerializer(many=True, read_only=True)
    image = serializers.CharField(source='get_image_url', read_only=True)
    absolute_url = serializers.CharField(source='get_absolute_url', read_only=True)
    activities = ActivitySerializer(many=True, read_only=True)
    opening_hours = OpeningHoursSerializer(many=True, read_only=True)

    class Meta:
        model = Business
        fields = [
            'id',
            'url',
            'user',
            'absolute_url',
            'image',
            'name',
            'latitude',
            'longitude',
            'addressGmap',
            'website',
            'phone',
            'business_email',
            'facebookUrl',
            'get_reviews_count',
            'get_rating',
            'opening_hours',
            'categories',
            'activities',

        ]


# ViewSets define the view behavior.
class BusinessViewSet(viewsets.ModelViewSet):
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    # filter_fields = ('name')


class BusinessList(generics.ListAPIView, ViewMixins):
    serializer_class = BusinessSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        query_set = Business.objects.filter(is_active=True)
        empty_array = []
        temp_empty_array = []
        location_array = []
        temp_location_array = []
        search_array = []
        temp_search_array = []
        search_location_array = []
        temp_search_location_array = []
        full_array = []
        temp_full_array = []
        category_array = []
        temp_category_array = []
        location_category_array = []
        temp_location_category_array = []
        search_category_array = []
        temp_search_category_array = []
        curr_array = []
        curr_empty_array = []
        temp_no_array = []
        no_array = []
        page_by = 6
        categories = Category.objects.filter(active=True)
        current_date = datetime.datetime.now().date()

        cat_query = self.request.query_params.get('category', None)
        cat_query = cat_query.split(",")

        lat_query = self.request.query_params.get('curr_lat', None)

        lng_query = self.request.query_params.get('curr_lng', None)

        search_query = self.request.query_params.get('search', None)

        location_query = self.request.query_params.get('location', None)

        latitude_query = self.request.query_params.get('latitude', None)

        longitude_query = self.request.query_params.get('longitude', None)

        dist_query = self.request.query_params.get('dist', None)

        page_by_query = self.request.query_params.get('page_by', None)

        ajax_key_query = self.request.query_params.get('ajax_key', None)
        # if cat_query is not None:
        #     cat_object = Category.objects.filter(slug=cat_query)
        #     if cat_object:
        #         query_set = query_set.filter(categories=cat_object)
        if ajax_key_query == 'location_key':
            for bsns in Business.objects.all():
                get_activity = Activity.objects.filter(business=bsns)
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    pt1 = geopy.Point(bsns.latitude, bsns.longitude)
                                    pt2 = geopy.Point(lat_query, lng_query)
                                    distance = geopy.distance.distance(pt1, pt2).km
                                    distance = round(distance)
                                    if int(distance) <= 100 and bsns not in temp_empty_array:
                                        empty_array.append(bsns)
                                        temp_empty_array.append(bsns)
            query_set = empty_array

        elif ajax_key_query == 'nolocation_key':
            for busins in Business.objects.filter(addressGmap__icontains='Dublin'):
                get_activity = Activity.objects.filter(business=busins)
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    if busins not in temp_no_array:
                                        no_array.append(busins)
                                        temp_no_array.append(busins)
            query_set = no_array

        elif ajax_key_query == 'filter_key':
            if search_query == '' and location_query != '' and cat_query == ['']:
                for buss in Business.objects.all():
                    get_activity = Activity.objects.filter(business=buss)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(buss.latitude, buss.longitude)
                                        pt2 = geopy.Point(latitude_query, longitude_query)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= int(dist_query) and buss not in temp_location_array:
                                            location_array.append(buss)
                                            temp_location_array.append(buss)
                query_set = location_array

            elif search_query != '' and location_query == '' and cat_query == ['']:
                for busines in Business.objects.all():
                    get_activity = Activity.objects.filter(
                        Q(name__icontains=search_query) | Q(business__name__icontains=search_query),
                        business=busines)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(busines.latitude, busines.longitude)
                                        pt2 = geopy.Point(lat_query, lng_query)
                                        distance = geopy.distance.distance(pt1, pt2).km

                                        distance = round(distance)
                                        if int(distance) <= int(dist_query) and busines not in temp_search_array:
                                            search_array.append(busines)
                                            temp_search_array.append(busines)
                query_set = search_array

            elif search_query != '' and location_query != '' and cat_query == ['']:
                for busines in Business.objects.all():
                    get_activity = Activity.objects.filter(
                        Q(name__icontains=search_query) | Q(business__name__icontains=search_query),
                        Q(business=busines))
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(busines.latitude, busines.longitude)
                                        pt2 = geopy.Point(latitude_query, longitude_query)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= int(
                                                dist_query) and busines not in temp_search_location_array:
                                            search_location_array.append(busines)
                                            temp_search_location_array.append(busines)
                query_set = search_location_array

            elif search_query != '' and location_query != '' and cat_query != ['']:
                for category in cat_query:
                    for business in Business.objects.filter(categories__name__icontains=category):
                        get_activity = Activity.objects.filter(
                            Q(name__icontains=search_query) | Q(business__name__icontains=search_query),
                            business=business)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(business.latitude, business.longitude)
                                            pt2 = geopy.Point(latitude_query, longitude_query)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(dist_query) and business not in temp_full_array:
                                                full_array.append(business)
                                                temp_full_array.append(business)
                query_set = full_array

            elif search_query == '' and location_query == '' and cat_query != ['']:
                for category in cat_query:
                    for busnss in Business.objects.filter(categories__name__icontains=category):
                        get_activity = Activity.objects.filter(business=busnss)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(busnss.latitude, busnss.longitude)
                                            pt2 = geopy.Point(lat_query, lng_query)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(dist_query) and busnss not in temp_category_array:
                                                category_array.append(busnss)
                                                temp_category_array.append(busnss)
                query_set = category_array

            elif search_query == '' and location_query != '' and cat_query != ['']:
                for category in cat_query:
                    for busnss in Business.objects.filter(categories__name__icontains=category):
                        get_activity = Activity.objects.filter(business=busnss)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(busnss.latitude, busnss.longitude)
                                            pt2 = geopy.Point(latitude_query, longitude_query)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(
                                                    dist_query) and busnss not in temp_location_category_array:
                                                location_category_array.append(busnss)
                                                temp_location_category_array.append(busnss)
                query_set = location_category_array

            elif search_query != '' and location_query == '' and cat_query != ['']:
                for category in cat_query:
                    for srch_busnss in Business.objects.filter(categories__name__icontains=category):
                        get_activity = Activity.objects.filter(
                            Q(name__icontains=search_query) | Q(business__name__icontains=search_query),
                            business=srch_busnss)
                        if get_activity:
                            # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                            for i in get_activity:
                                for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                    array = []
                                    temp_array = []
                                    end_repeat = str(i.end_repeat)
                                    for end in end_repeat.split(','):
                                        if end not in temp_array:
                                            array.append(end)
                                            temp_array.append(end)
                                    try:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                    except:
                                        sort_array = sorted(array,
                                                            key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                    for s_array in sort_array[-1:]:
                                        try:
                                            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime(
                                                '%Y-%m-%d')
                                        except:
                                            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime(
                                                '%Y-%m-%d')
                                        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                        if n_date >= current_date:
                                            pt1 = geopy.Point(srch_busnss.latitude, srch_busnss.longitude)
                                            pt2 = geopy.Point(lat_query, lng_query)
                                            distance = geopy.distance.distance(pt1, pt2).km
                                            distance = round(distance)
                                            if int(distance) <= int(
                                                    dist_query) and srch_busnss not in temp_search_category_array:
                                                search_category_array.append(srch_busnss)
                                                temp_search_category_array.append(srch_busnss)
                query_set = search_category_array

            elif search_query == '' and location_query == '' and cat_query == ['']:
                for bsns in Business.objects.all():
                    get_activity = Activity.objects.filter(business=bsns)
                    if get_activity:
                        # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                        for i in get_activity:
                            for i in RecurringService.objects.filter(activity=i, is_archived=False):
                                array = []
                                temp_array = []
                                end_repeat = str(i.end_repeat)
                                for end in end_repeat.split(','):
                                    if end not in temp_array:
                                        array.append(end)
                                        temp_array.append(end)
                                try:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                                except:
                                    sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                                for s_array in sort_array[-1:]:
                                    try:
                                        s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                    except:
                                        s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                    n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                    if n_date >= current_date:
                                        pt1 = geopy.Point(bsns.latitude, bsns.longitude)
                                        pt2 = geopy.Point(lat_query, lng_query)
                                        distance = geopy.distance.distance(pt1, pt2).km
                                        distance = round(distance)
                                        if int(distance) <= 100 and bsns not in curr_empty_array:
                                            curr_array.append(bsns)
                                            curr_empty_array.append(bsns)
                query_set = curr_array

        elif ajax_key_query == 'none_key':
            query_set = Business.objects.filter(addressGmap__icontains='Dublin')

        query_array = []
        for item in query_set:
            query_array.append(item.id)
        if page_by_query == 'undefined':
            query_set = Business.objects.filter(id__in=query_array)

        elif page_by_query != 'undefined':
            total = page_by * (int(page_by_query) - 1)
            query_set = Business.objects.filter(id__in=query_array)[:total]

        return query_set


class BusinessDetail(generics.RetrieveAPIView):
    serializer_class = BusinessSerializer
    queryset = Business.objects.all()

# class OpeningHoursSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = OpeningHours
#         fields = ('url', 'activity', 'weekday')

# # ViewSets define the view behavior.
# class OpeningHoursViewSet(viewsets.ModelViewSet):
#     queryset = OpeningHours.objects.all()
#     serializer_class = OpeningHoursSerializer
