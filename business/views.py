import datetime

from allauth.socialaccount.providers.facebook.views import login_by_token
from django.views.generic import ListView
from pytz import timezone
# import GoogleMaps as GoogleMaps
from django.db.models import Q, Sum

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.utils.timezone import activate
from django.views.generic.base import TemplateView
from django.core.cache import cache

from bookings.models import Booking, TransactionHistory
from business.mixins import ViewMixins
from services.models import Service, RecurringService
from spotimist import settings
from .forms import BusinessSignupForm, BusinessUpdateForm, BusinessImageForm, OpeningHoursForm
from .models import Business, BusinessImage, OpeningHours
from activities.models import Activity
from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.views import ConfirmEmailView
from categories.models import Category
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from reviews.models import Review
import json
import logging
import geopy
import geopy.distance
import socket

# Create your views here.

User = get_user_model()

# Get an instance of a logger
logger = logging.getLogger(__name__)


@login_required
def business_create(request):
    # from accounts.forms import SignupForm
    bform = BusinessSignupForm(request.POST or None, request.FILES or None)
    image_form = BusinessImageForm(request.POST or None, request.FILES or None)
    # uform = SignupForm(request.POST or None, request.FILES or None)

    # check if user already has business
    if request.method != "POST":
        if Business.objects.filter(user=request.user).exists():
            raise Http404("You can't access this object.")
    else:

        if bform.is_valid():
            address = request.POST.get('address')
            country = request.POST.get('country')
            latitude = request.POST.get('latitude')
            postCode = request.POST.get('postCode')
            longitude = request.POST.get('longitude')
            instance_b = bform.save(commit=False)
            instance_b.user = request.user
            instance_b.address = address
            instance_b.country = country
            instance_b.postCode = postCode
            instance_b.latitude = latitude
            instance_b.longitude = longitude
            instance_b.save()
            bform.save_m2m()

            messages.success(request, 'Business created succesfully', extra_tags="business_created")
            return HttpResponseRedirect(reverse("business:profile-edit"))

    context = {
        "form": bform
        # "uform": uform
    }
    return render(request, "business/register.html", context)


def business_activities(request):
    # business = Business.objects.filter(user=request.user)
    business = get_object_or_404(Business, user=request.user)

    context = {
        "business": business,
    }
    return render(request, "business/index.html", context)


def business_detail(request, slug):
    business = get_object_or_404(Business, slug=slug)
    activities = Activity.objects.filter(business=business)
    business_images = business.images.all()
    opening_hours = business.opening_hours.all()
    reviews = business.reviews.all()
    activity_service = []
    for i in activities:
        activity_service.append(i.get_services_list())

    now_time = datetime.datetime.now(timezone(str(settings.TIME_ZONE)))
    hour_time = now_time.time()

    context = {
        "business": business,
        "activities": activities,
        "images": business_images,
        "opening_hours": opening_hours,
        "reviews": reviews,
        "hour": hour_time,
        "day": now_time.strftime("%A"),
        "activity_service": activity_service,
    }

    return render(request, "business/index.html", context)


def business_profile_edit(request):
    business = get_object_or_404(Business, user=request.user)
    form = BusinessUpdateForm(request.POST or None, request.FILES or None, instance=business)
    image_form = BusinessImageForm(request.POST or None, request.FILES or None)

    def openings(business, weekday, from_hour, to_hour, existing_hours=None):
        """

        :param business: bussiness object
        :param weekday: week id
        :param from_hour: opening hour
        :param to_hour: closing hour
        :param existing_hours: existing data with above parameters
        :return:
        """
        if not existing_hours:
            opening_hours = OpeningHours()
            opening_hours.business = business
            opening_hours.weekday = weekday
            opening_hours.from_hour = from_hour
            opening_hours.to_hour = to_hour
            try:
                if opening_hours.from_hour < opening_hours.to_hour:
                    opening_hours.save()
            except:
                pass
        else:
            existing_hours.business = business
            existing_hours.weekday = weekday
            existing_hours.from_hour = from_hour
            existing_hours.to_hour = to_hour
            if existing_hours.from_hour < existing_hours.to_hour:
                existing_hours.save()

    if request.method == 'POST':
        opening_hours_form = OpeningHoursForm(request.POST)
        import dateutil.parser
        monday_opening = request.POST.get('monday_opening')
        monday_closing = request.POST.get('monday_closing')
        tuesday_opening = request.POST.get('tuesday_opening')
        tuesday_closing = request.POST.get('tuesday_closing')
        wednesday_opening = request.POST.get('wednesday_opening')
        wednesday_closing = request.POST.get('wednesday_closing')
        thursday_opening = request.POST.get('thursday_opening')
        thursday_closing = request.POST.get('thursday_closing')
        friday_opening = request.POST.get('friday_opening')
        friday_closing = request.POST.get('friday_closing')
        saturday_opening = request.POST.get('saturday_opening')
        saturday_closing = request.POST.get('saturday_closing')
        sunday_opening = request.POST.get('sunday_opening')
        sunday_closing = request.POST.get('sunday_closing')
        if monday_opening:
            try:
                if monday_opening != 'Closed' and monday_closing != 'Closed':
                    if OpeningHours.objects.get(business=business.id, weekday=1):
                        existing_hours = OpeningHours.objects.get(business=business.id, weekday=1)
                        openings(business, 1, dateutil.parser.parse(monday_opening),
                                 dateutil.parser.parse(monday_closing), existing_hours)
            except:
                datetime_from = dateutil.parser.parse(monday_opening)
                datetime_to = dateutil.parser.parse(monday_closing)
                openings(business, 1, datetime_from, datetime_to)
        if tuesday_opening:
            try:
                if tuesday_opening != 'Closed' and tuesday_closing != 'Closed':
                    if OpeningHours.objects.get(business=business.id, weekday=2):
                        existing_hours = OpeningHours.objects.get(business=business.id, weekday=2)
                        openings(business, 2, dateutil.parser.parse(tuesday_opening),
                                 dateutil.parser.parse(tuesday_closing), existing_hours)
            except:
                datetime_from = dateutil.parser.parse(tuesday_opening)
                datetime_to = dateutil.parser.parse(tuesday_closing)
                openings(business, 2, datetime_from, datetime_to)
        if wednesday_opening:
            try:
                if wednesday_opening != 'Closed' and wednesday_closing != 'Closed':
                    if OpeningHours.objects.get(business=business.id, weekday=3):
                        existing_hours = OpeningHours.objects.get(business=business.id, weekday=3)
                        openings(business, 3, dateutil.parser.parse(wednesday_opening),
                                 dateutil.parser.parse(wednesday_closing), existing_hours)
            except:
                datetime_from = dateutil.parser.parse(wednesday_opening)
                datetime_to = dateutil.parser.parse(wednesday_closing)
                openings(business, 3, datetime_from, datetime_to)

        if thursday_opening:
            try:
                if thursday_opening != 'Closed' and thursday_closing != 'Closed':
                    if OpeningHours.objects.get(business=business.id, weekday=4):
                        existing_hours = OpeningHours.objects.get(business=business.id, weekday=4)
                        openings(business, 4, dateutil.parser.parse(thursday_opening),
                                 dateutil.parser.parse(thursday_closing), existing_hours)
            except:
                datetime_from = dateutil.parser.parse(thursday_opening)
                datetime_to = dateutil.parser.parse(thursday_closing)
                openings(business, 4, datetime_from, datetime_to)

        if friday_opening:
            try:
                if friday_opening != 'Closed' and friday_closing != 'Closed':
                    if OpeningHours.objects.get(business=business.id, weekday=5):
                        existing_hours = OpeningHours.objects.get(business=business.id, weekday=5)
                        openings(business, 5, dateutil.parser.parse(friday_opening),
                                 dateutil.parser.parse(friday_closing), existing_hours)
            except:
                datetime_from = dateutil.parser.parse(friday_opening)
                datetime_to = dateutil.parser.parse(friday_closing)
                openings(business, 5, datetime_from, datetime_to)

        if saturday_opening:
            try:
                if saturday_opening != 'Closed' and saturday_closing != 'Closed':
                    if OpeningHours.objects.get(business=business.id, weekday=6):
                        existing_hours = OpeningHours.objects.get(business=business.id, weekday=6)
                        openings(business, 6, dateutil.parser.parse(saturday_opening),
                                 dateutil.parser.parse(saturday_closing), existing_hours)
            except:
                datetime_from = dateutil.parser.parse(saturday_opening)
                datetime_to = dateutil.parser.parse(saturday_closing)
                openings(business, 6, datetime_from, datetime_to)

        if sunday_opening:
            try:
                if sunday_opening != 'Closed' and sunday_closing != 'Closed':
                    if OpeningHours.objects.get(business=business.id, weekday=7):
                        existing_hours = OpeningHours.objects.get(business=business.id, weekday=7)
                        openings(business, 7, dateutil.parser.parse(sunday_opening),
                                 dateutil.parser.parse(sunday_closing), existing_hours)
            except:
                datetime_from = dateutil.parser.parse(sunday_opening)
                datetime_to = dateutil.parser.parse(sunday_closing)
                openings(business, 7, datetime_from, datetime_to)

        if form.is_valid():

            if request.POST['check_status'] == 'unchecked':
                business.check_status = False
                business.save()
            elif request.POST['check_status'] == 'checked':
                business.check_status = True
                business.save()
            instance = form.save()
            # message success
            # messages.success(request, "Successfully Updated", extra_tags="some-tag")
            # return HttpResponseRedirect(activity.get_absolute_url())

    else:
        opening_hours_form = OpeningHoursForm()

    # Getting all business images to create JSON string (to be used by dropzone later for displaying existing images)
    json_array = []

    business_images = business.images.all()
    for business_image in business_images:
        try:

            to_json = {
                "name": business_image.image.name,
                "size": business_image.image.size,
                "url": business_image.image.url
            }
            json_array.append(to_json)
        except Exception as e:
            logger.error('Business image error ' + str(e))

    open_hr = business.opening_hours.all()

    def open_hours(weekday):
        """

        :param weekday: weekday id
        :return: opening hour for corresponding week id
        """
        try:
            open_hour = open_hr.get(weekday=weekday).from_hour
        except:
            open_hour = 'Closed'
        return open_hour

    def close_hours(weekday):
        """

        :param weekday: weekday id
        :return: closing hour for corresponding week id
        """
        try:
            close_hour = open_hr.get(weekday=weekday).to_hour
        except:
            close_hour = 'Closed'
        return close_hour

    context = {
        "business": business,
        "form": form,
        "json_images": json.dumps(json_array),
        "opening_hours_form": opening_hours_form,
        "errors": opening_hours_form.errors,
        "monday_opn": open_hours(1),
        "monday_cls": close_hours(1),
        "tuesday_opn": open_hours(2),
        "tuesday_cls": close_hours(2),
        "wednesday_opn": open_hours(3),
        "wednesday_cls": close_hours(3),
        "thursday_opn": open_hours(4),
        "thursday_cls": close_hours(4),
        "friday_opn": open_hours(5),
        "friday_cls": close_hours(5),
        "saturday_opn": open_hours(6),
        "saturday_cls": close_hours(6),
        "sunday_opn": open_hours(7),
        "sunday_cls": close_hours(7),

    }
    return render(request, "business/edit.html", context)


def upload_business_image(request):
    business = get_object_or_404(Business, user=request.user)

    # files = [request.FILES.get('file[%d]' % i) for i in range(0, len(request.FILES))]

    try:
        files = [request.FILES.get('file')]

        for f in files:
            business_image = BusinessImage.objects.create(
                business=business,
                image=f)
            if not business.image:
                business.image = f
                business.save()

        return HttpResponse("OK")
    except Exception as e:
        print("error in upload " + str(e))


@login_required
def delete_business_image(request):
    image_name = request.POST.get("file_d")

    if request.method == 'POST':
        try:
            business = get_object_or_404(Business, user=request.user)
            business_image = business.images.filter(image=image_name)
            business_image.delete()

            return HttpResponse("OK")
        except Exception as e:
            print("error when deleting image " + str(e))
            return HttpResponse("KO")


class FilterView(ListView, ViewMixins):
    template_name = "business/list.html"
    model = Business
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super(FilterView, self).get_context_data(**kwargs)
        context["api_url"] = reverse('api-business-list')
        context["category"] = self.request.GET.get('listing_categories', '')
        context["destination"] = self.request.GET.get('location_search', '')
        context["search"] = self.request.GET.get('data_search', '')
        context["home_lat"] = self.request.GET.get('home_lat', '')
        context["home_lng"] = self.request.GET.get('home_lng', '')
        context["ajax_key"] = self.request.GET.get('ajax_key', '')
        context["categories"] = Category.objects.filter(active=True)

        cat_query = self.request.GET.get('listing_categories', '')
        cat_object = Category.objects.filter(slug=cat_query)
        loc_query = self.request.GET.get('location_search', '')
        dat_query = self.request.GET.get('data_search', '')
        home_lat = self.request.GET.get('home_lat', '')
        home_lng = self.request.GET.get('home_lng', '')
        context['object_list'] = self.home_filter(cat_object, dat_query, loc_query, home_lat, home_lng)
        if context['object_list'] == '':
            current_date = datetime.datetime.now().date()
            temp_no_array = []
            no_array = []
            for busins in Business.objects.filter(addressGmap__icontains='Dublin'):
                get_activity = Activity.objects.filter(business=busins)
                if get_activity:
                    # if RecurringService.objects.filter(activity=get_activity, end_repeat__gte=current_date):
                    for i in get_activity:
                        for i in RecurringService.objects.filter(activity=i, is_archived=False):
                            array = []
                            temp_array = []
                            end_repeat = str(i.end_repeat)
                            for end in end_repeat.split(','):
                                if end not in temp_array:
                                    array.append(end)
                                    temp_array.append(end)
                            try:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
                            except:
                                sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
                            for s_array in sort_array[-1:]:
                                try:
                                    s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                                except:
                                    s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                                n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
                                if n_date >= current_date:
                                    if busins not in temp_no_array:
                                        no_array.append(busins)
                                        temp_no_array.append(busins)
            context['object_list'] = no_array
        return context

    def post(self, request):
        response_data = {}
        search = request.POST.get('search')
        location = request.POST.get('location')
        categories = request.POST.getlist('category[]')
        if not categories:
            categories = request.POST.get('category')
        dist = request.POST.get('dist')
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')
        ajax_key = request.POST.get('ajax_key')
        curr_lat = request.POST.get('curr_lat')
        curr_lng = request.POST.get('curr_lng')
        page_by = request.POST.get('page_by')
        if not page_by:
            page_by = 1
        else:
            page_by = int(page_by)
        if ajax_key == 'page_key' and (search != '' or location != '' or categories != ''):
            ajax_key = 'filter_key'
        if ajax_key == 'page_key' and curr_lat == 53.3498053:
            ajax_key = 'nolocation_key'
        elif ajax_key == 'page_key' and curr_lat != 53.3498053:
            ajax_key = 'location_key'

        query_set = self.business_filter(ajax_key, search, location, categories, dist, latitude, longitude, curr_lat,
                                         curr_lng)

        now_time = datetime.datetime.now(timezone(str(settings.TIME_ZONE)))
        hour_time = now_time.time()
        page_size = self.get_paginate_by(query_set)
        paginator, page, queryset, is_paginated = self.paginate_queryset(query_set, page_size)
        page = paginator.page(page_by)
        queryset = paginator.page(page_by).object_list

        context = {
            "object_list": queryset,
            "title": "Businesses List",
            "activities_count": len(query_set),
            "hour": hour_time,
            "day": now_time.strftime("%A"),
            # "selected_category": cat_query,
            "page_obj": page,
        }
        response_data['business_data'] = render_to_string('ajax/business_list.html', context)
        response_data['business_list_data'] = render_to_string('ajax/business_list_inner.html', context)
        response_data['page_data'] = render_to_string('ajax/pagination.html', context)

        return HttpResponse(json.dumps(response_data), content_type="application/json")

@login_required
def dashboard_get_app(request):

    if not request.user.has_business():
        raise Http404("You can't access this object.")

    return render(request, "mobile-app.html")