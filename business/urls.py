"""
URLs configuration for Activities 

"""
from django.conf.urls import url
from allauth.account.views import LoginView, SignupView 
from . import views

urlpatterns = [
    url(r'^activities/$', views.business_activities, name="activities"),
    url(r'^register/$', views.business_create, name="create"),
    url(r'^profile/$', views.business_profile_edit, name="profile-edit"),
    url(r'^business-image-delete/$', views.delete_business_image, name="business-image-delete"),
    url(r'^business-image-upload/$', views.upload_business_image, name="business-image-upload"),
]