import os
from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from django.template.loader import get_template
from django.utils.text import slugify
from django.utils.translation import gettext as _


# Create your models here.

class Business(models.Model):
    """Model definition for Business."""

    name = models.CharField(max_length=120)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="business")
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(blank=True, null=True, width_field="width_field", height_field="height_field")
    height_field = models.IntegerField(default=0, null=True)
    width_field = models.IntegerField(default=0, null=True)
    addressGmap = models.CharField(max_length=250)
    address = models.CharField(max_length=250, blank=True, null=True)
    postCode = models.CharField(max_length=120, blank=True, null=True)
    city = models.CharField(max_length=120, blank=True, null=True)
    country = models.CharField(max_length=120, blank=True, null=True)
    website = models.CharField(max_length=120, blank=True, null=True)
    phone = models.CharField(max_length=120, blank=True, null=True)
    business_email = models.EmailField(max_length=120, blank=True, null=True)
    facebookUrl = models.CharField(max_length=250, blank=True, null=True)
    twitterUrl = models.CharField(max_length=250, blank=True, null=True)
    instagramUrl = models.CharField(max_length=250, blank=True, null=True)
    youtubeUrl = models.CharField(max_length=250, blank=True, null=True)
    linkedInUrl = models.CharField(max_length=250, blank=True, null=True)
    latitude = models.CharField(max_length=120, blank=True, null=True)
    longitude = models.CharField(max_length=120, blank=True, null=True)
    slug = models.SlugField(unique=True, editable=False)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    categories = models.ManyToManyField('categories.Category', blank=True)
    is_active = models.BooleanField(default=False)
    check_status = models.BooleanField(default=True)
    booking_fee = models.IntegerField(blank=True, null=True)

    class Meta:
        """Meta definition for Business."""

        verbose_name = 'Business'
        verbose_name_plural = 'Businesses'
        ordering = ['-id']

    def __str__(self):
        return self.name

    def set_status(self, status):
        self.is_active = status
        self.save()

    def get_absolute_url(self):
        try:
            return reverse("business-detail", kwargs={"slug": self.slug})
        except Exception as e:
            print("Error on get_absolute_url " + str(e))

    def get_first_category(self):
        cat = self.categories.first()
        return cat
    
    def get_activities(self):
        test = self.activities.all()
        return test

    # def get_edit_url(self):
    #     return reverse ("activities:edit", kwargs={"slug": self.slug})

    # def get_delete_url(self):
    #     return reverse ("activities:delete", kwargs={"slug": self.slug})

    def get_image_url(self):
        if self.image:
            image_url = self.image.url
        else:
            image_url = settings.STATIC_URL + "Old/img/placeholder_2.jpg"

        return image_url

    def get_rating(self):
        from reviews.models import Review
        reviews = Review.objects.filter(business=self)
        total_ratings = 0
        reviews_average = 0
        reviews_count = reviews.count()
        if reviews_count > 0:
            for review in reviews:
                total_ratings += review.rating
            reviews_average = total_ratings / reviews_count

        return round(reviews_average, 1)

    def get_reviews_count(self):
        from reviews.models import Review
        reviews = Review.objects.filter(business=self)
        return reviews.count()

def create_slug(instance, new_slug=None):
    slug = slugify(instance.name)
    if new_slug is not None:
        slug = new_slug
    qs = Business.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


@receiver(pre_save, sender=Business)
def pre_save_activity_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)


class BusinessImage(models.Model):
    business = models.ForeignKey(Business, related_name='images')
    is_profile = models.BooleanField(default=False)
    image = models.ImageField()


@receiver(post_delete, sender=BusinessImage)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


# """ 
# This receiver is triggered every time the business is saved
# The reason is that before the saving process the image hasn't been uploaded to filesystem there for image name will reflect the name of the file the user is uploading 
# rather than the actual name that the application will store on file system.
# Also, we need to make sure that everytime the business is updated then we update the marker content value otherwise the map will keep displaying old data (e.g. Image/name)
# """
# @receiver(post_save, sender=Business)
# def marker_handler(sender, **kwargs):
#     business = kwargs.get('instance')

#     ## Reading the template that needs to be injected in the markers to be displayed in the map
#     template_string = get_template('activity/marker_content.html').render()
#     content = template_string.replace('[[name]]', business.name).replace('[[link]]', business.get_absolute_url()).replace('[[image]]', business.get_image_url())
#     marker_content = '<div class="marker-inventor-poi"><div class="marker-inventor-poi-inner"><i class="inventor-poi inventor-poi-house"></i></div></div>'

#     if kwargs.get('created', False):
#         business.content = content
#         business.marker_content = marker_content
#         business.save()
#     else:
#         business.content = template_string.replace('[[name]]', business.name).replace('[[link]]', business.get_absolute_url()).replace('[[image]]', business.get_image_url())
#         marker_content = '<div class="marker-inventor-poi"><div class="marker-inventor-poi-inner"><i class="inventor-poi inventor-poi-house"></i></div></div>'
#         Business.objects.filter(id=business.id).update(content=content, marker_content=marker_content)


# pre_save.connect(pre_save_activity_receiver, sender=Business)


WEEKDAYS = [
    (1, _("Monday")),
    (2, _("Tuesday")),
    (3, _("Wednesday")),
    (4, _("Thursday")),
    (5, _("Friday")),
    (6, _("Saturday")),
    (7, _("Sunday")),
]


class OpeningHours(models.Model):
    business = models.ForeignKey(Business, related_name="opening_hours")
    weekday = models.IntegerField(choices=WEEKDAYS)
    from_hour = models.TimeField()
    to_hour = models.TimeField()

    # closed status

    class Meta:
        ordering = ('weekday', 'from_hour')
        unique_together = ('weekday', 'from_hour', 'to_hour')

    def __str__(self):
        return u'%s: %s - %s' % (self.get_weekday_display(), self.from_hour, self.to_hour)
