import datetime

from django import template

from activities.models import Activity
from business.models import Business
from services.models import Service, RecurringService

register = template.Library()


@register.simple_tag(name='spot_time')
def spot_time(value, day, hour):
    string = ''
    for hours in value:
        if hours.get_weekday_display() == day and hours.from_hour <= hour and hours.to_hour >= hour:

            string = 'Now Open'
            break
        else:
            string = "Closed"

    return string


@register.simple_tag(name='avail_service')
def avail_service(value):
    service_avail = RecurringService.objects.get(id=value.id)
    array = []
    temp_array = []
    end_repeat = str(service_avail.end_repeat)
    current_date = datetime.datetime.now().date()
    for end in end_repeat.split(','):
        if end not in temp_array:
            array.append(end)
            temp_array.append(end)
    try:
        sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
    except:
        sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
    for s_array in sort_array[-1:]:
        try:
            s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
        except:
            s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
        n_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
        if n_date >= current_date:
            string = "available"
        else:
            string = "not-available"
        return string
