import datetime
from django import forms
from django.forms import ModelForm
from .models import Business, BusinessImage
from django.utils.translation import ugettext_lazy as _


# Create the form Class.
class BusinessSignupForm(ModelForm):
    accept_tc = forms.BooleanField()

    class Meta:
        model = Business
        fields = [
            "name",
            "business_email",
            "addressGmap",
            "latitude",
            "longitude",
            "address",
            "postCode",
            "city",
            "country",
            "categories",
            "accept_tc"
        ]
        labels = {
            "name": _("Business Name"),
            "addressGmap": _("Address")
        }
        widgets = {
            "name": forms.TextInput(attrs={'class': 'form-control'}),
            "email": forms.EmailInput(attrs={'class': 'form-control', 'name': 'b_email'}),
            "addressGmap": forms.TextInput(attrs={'id': 'autocomplete'}),
            "latitude": forms.HiddenInput(),
            "longitude": forms.HiddenInput(),
            "address": forms.HiddenInput(),
            "postCode": forms.HiddenInput(),
            "city": forms.HiddenInput(),
            "country": forms.HiddenInput(),
            "categories": forms.CheckboxSelectMultiple(),
            "accept_tc": forms.CheckboxSelectMultiple()
        }


class BusinessUpdateForm(ModelForm):
    class Meta:
        model = Business
        fields = [
            "name",
            "description",
            "image",
            "business_email",
            "website",
            "phone",
            "categories",
            "latitude",
            "longitude",
            "address",
            "postCode",
            "city",
            "country",
            "addressGmap",
            "facebookUrl",
            "twitterUrl",
            "instagramUrl",
            "check_status"
        ]
        labels = {
            "name": _("Business Name"),
            "addressGmap": _("Address")
        }
        widgets = {
            "name": forms.TextInput(attrs={'class': 'search-field'}),
            "latitude": forms.HiddenInput(),
            "longitude": forms.HiddenInput(),
            "categories": forms.CheckboxSelectMultiple(),
            "address": forms.HiddenInput(),
            "postCode": forms.HiddenInput(),
            "city": forms.HiddenInput(),
            "country": forms.HiddenInput(),
            "categories": forms.CheckboxSelectMultiple(),
            "addressGmap": forms.TextInput(attrs={'id': 'autocomplete'}),
            "facebookUrl": forms.TextInput(attrs={'placeholder': 'https://www.facebook.com/'}),
            "twitterUrl": forms.TextInput(attrs={'placeholder': 'https://www.twitter.com/'}),
            "instagramUrl": forms.TextInput(attrs={'placeholder': 'https://www.instagram.com/'}),
            "check_status": forms.HiddenInput()
        }

    def clean_description(self):
        description = self.cleaned_data.get('description')
        description_length = 150
        if len(description) < description_length:
            raise forms.ValidationError("Description content should be minimum 150 characters...!")
        return description


class BusinessImageForm(ModelForm):
    class Meta:
        model = BusinessImage
        fields = ["business"]


OPENING_HOURS_CHOICES = [
    ('Closed', _("Closed")),
    ('00:00:00', _("12:00 AM")),
    ('00:30:00', _("12:30 AM")),
    ('01:00:00', _("01:00 AM")),
    ('01:30:00', _("01:30 AM")),
    ('02:00:00', _("02:00 AM")),
    ('02:30:00', _("02:30 AM")),
    ('03:00:00', _("03:00 AM")),
    ('03:30:00', _("03:30 AM")),
    ('04:00:00', _("04:00 AM")),
    ('04:30:00', _("04:30 AM")),
    ('05:00:00', _("05:00 AM")),
    ('05:30:00', _("05:30 AM")),
    ('06:00:00', _("06:00 AM")),
    ('06:30:00', _("06:30 AM")),
    ('07:00:00', _("07:00 AM")),
    ('07:30:00', _("07:30 AM")),
    ('08:00:00', _("08:00 AM")),
    ('08:30:00', _("08:30 AM")),
    ('09:00:00', _("09:00 AM")),
    ('09:30:00', _("09:30 AM")),
    ('10:00:00', _("10:00 AM")),
    ('10:30:00', _("10:30 AM")),
    ('11:00:00', _("11:00 AM")),
    ('11:30:00', _("11:30 AM")),
    ('12:00:00', _("12:00 PM")),
    ('12:30:00', _("12:30 PM")),
    ('13:00:00', _("01:00 PM")),
    ('13:30:00', _("01:30 PM")),
    ('14:00:00', _("02:00 PM")),
    ('14:30:00', _("02:30 PM")),
    ('15:00:00', _("03:00 PM")),
    ('15:30:00', _("03:30 PM")),
    ('16:00:00', _("04:00 PM")),
    ('16:30:00', _("04:30 PM")),
    ('17:00:00', _("05:00 PM")),
    ('17:30:00', _("05:30 PM")),
    ('18:00:00', _("06:00 PM")),
    ('18:30:00', _("06:30 PM")),
    ('19:00:00', _("07:00 PM")),
    ('19:30:00', _("07:30 PM")),
    ('20:00:00', _("08:00 PM")),
    ('20:30:00', _("08:30 PM")),
    ('21:00:00', _("09:00 PM")),
    ('21:30:00', _("09:30 PM")),
    ('22:00:00', _("10:00 PM")),
    ('22:30:00', _("10:30 PM")),
    ('23:00:00', _("11:00 PM")),
    ('23:30:00', _("11:30 PM")),
]


class OpeningHoursForm(forms.Form):
    monday_opening = forms.ChoiceField(choices=OPENING_HOURS_CHOICES, widget=forms.Select(
        attrs={'class': 'chosen-select', 'data-placeholder': 'Opening Time'}))
    monday_closing = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    tuesday_opening = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    tuesday_closing = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    wednesday_opening = forms.ChoiceField(choices=OPENING_HOURS_CHOICES, widget=forms.Select(
        attrs={'class': 'chosen-select', 'data-placeholder': 'Opening Time'}))
    wednesday_closing = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    thursday_opening = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    thursday_closing = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    friday_opening = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    friday_closing = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    saturday_opening = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    saturday_closing = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    sunday_opening = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)
    sunday_closing = forms.ChoiceField(choices=OPENING_HOURS_CHOICES)

    def clean_monday_closing(self):
        monday_opening = self.cleaned_data.get('monday_opening')
        monday_closing = self.cleaned_data.get('monday_closing')
        if monday_opening == 'Closed' and monday_closing != 'Closed' or monday_opening != 'Closed' and monday_closing == 'Closed':
            raise forms.ValidationError("Invalid data...!")

        if monday_opening != 'Closed' or monday_closing != 'Closed':
            monday_opening = datetime.datetime.strptime(monday_opening, '%H:%M:%S')
            monday_closing = datetime.datetime.strptime(monday_closing, '%H:%M:%S')
            if monday_opening > monday_closing:
                raise forms.ValidationError("Invalid data...!")
        return monday_opening

    def clean_tuesday_closing(self):
        tuesday_opening = self.cleaned_data.get('tuesday_opening')
        tuesday_closing = self.cleaned_data.get('tuesday_closing')
        if tuesday_opening == 'Closed' and tuesday_closing != 'Closed' or tuesday_opening != 'Closed' and tuesday_closing == 'Closed':
            raise forms.ValidationError("Invalid data...!")

        elif tuesday_opening != 'Closed' or tuesday_closing != 'Closed':
            tuesday_opening = datetime.datetime.strptime(tuesday_opening, '%H:%M:%S')
            tuesday_closing = datetime.datetime.strptime(tuesday_closing, '%H:%M:%S')
            if tuesday_opening > tuesday_closing:
                raise forms.ValidationError("Invalid data...!")
        return tuesday_opening

    def clean_wednesday_closing(self):
        wednesday_opening = self.cleaned_data.get('wednesday_opening')
        wednesday_closing = self.cleaned_data.get('wednesday_closing')
        if wednesday_opening == 'Closed' and wednesday_closing != 'Closed' or wednesday_opening != 'Closed' and wednesday_closing == 'Closed':
            raise forms.ValidationError("Invalid data...!")

        elif wednesday_opening != 'Closed' or wednesday_closing != 'Closed':
            wednesday_opening = datetime.datetime.strptime(wednesday_opening, '%H:%M:%S')
            wednesday_closing = datetime.datetime.strptime(wednesday_closing, '%H:%M:%S')
            if wednesday_opening > wednesday_closing:
                raise forms.ValidationError("Invalid data...!")
        return wednesday_opening

    def clean_thursday_closing(self):
        thursday_opening = self.cleaned_data.get('thursday_opening')
        thursday_closing = self.cleaned_data.get('thursday_closing')
        if thursday_opening == 'Closed' and thursday_closing != 'Closed' or thursday_opening != 'Closed' and thursday_closing == 'Closed':
            raise forms.ValidationError("Invalid data...!")

        elif thursday_opening != 'Closed' or thursday_closing != 'Closed':
            thursday_opening = datetime.datetime.strptime(thursday_opening, '%H:%M:%S')
            thursday_closing = datetime.datetime.strptime(thursday_closing, '%H:%M:%S')
            if thursday_opening > thursday_closing:
                raise forms.ValidationError("Invalid data...!")
        return thursday_opening

    def clean_friday_closing(self):
        friday_opening = self.cleaned_data.get('friday_opening')
        friday_closing = self.cleaned_data.get('friday_closing')
        if friday_opening == 'Closed' and friday_closing != 'Closed' or friday_opening != 'Closed' and friday_closing == 'Closed':
            raise forms.ValidationError("Invalid data...!")

        elif friday_opening != 'Closed' or friday_closing != 'Closed':
            friday_opening = datetime.datetime.strptime(friday_opening, '%H:%M:%S')
            friday_closing = datetime.datetime.strptime(friday_closing, '%H:%M:%S')

            if friday_opening > friday_closing:
                raise forms.ValidationError("Invalid data...!")
        return friday_opening

    def clean_saturday_closing(self):
        saturday_opening = self.cleaned_data.get('saturday_opening')
        saturday_closing = self.cleaned_data.get('saturday_closing')
        if saturday_opening == 'Closed' and saturday_closing != 'Closed' or saturday_opening != 'Closed' and saturday_closing == 'Closed':
            raise forms.ValidationError("Invalid data...!")

        elif saturday_opening != 'Closed' or saturday_closing != 'Closed':
            saturday_opening = datetime.datetime.strptime(saturday_opening, '%H:%M:%S')
            saturday_closing = datetime.datetime.strptime(saturday_closing, '%H:%M:%S')

            if saturday_opening > saturday_closing:
                raise forms.ValidationError("Invalid data...!")
        return saturday_opening

    def clean_sunday_closing(self):
        sunday_opening = self.cleaned_data.get('sunday_opening')
        sunday_closing = self.cleaned_data.get('sunday_closing')
        if sunday_opening == 'Closed' and sunday_closing != 'Closed' or sunday_opening != 'Closed' and sunday_closing == 'Closed':
            raise forms.ValidationError("Invalid data...!")
        elif sunday_opening != 'Closed' or sunday_closing != 'Closed':
            sunday_opening = datetime.datetime.strptime(sunday_opening, '%H:%M:%S')
            sunday_closing = datetime.datetime.strptime(sunday_closing, '%H:%M:%S')
            if sunday_opening > sunday_closing:
                raise forms.ValidationError("Invalid data...!")

        return sunday_opening
