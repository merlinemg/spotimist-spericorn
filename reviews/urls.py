"""
URLs configuration for Reviews 

"""
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^my-reviews/$', views.reviews_list, name="list_my_reviews"),
    url(r'^business-reviews/$', views.reviews_list, name="list_business_reviews"),
    url(r'^create/(?P<pk>\d+)/$', views.create_review, name="create"),
    url(r'^(?P<pk>\d+)/edit$', views.edit_review, name="edit"),
    url(r'^(?P<pk>\d+)/delete/$', views.delete_review, name="delete"),
]