# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-02-06 11:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0003_review_helpful_count'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='business',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='business.Business'),
        ),
    ]
