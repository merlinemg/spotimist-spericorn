from django import forms
from .models import Review
from django.utils.translation import gettext_lazy as _

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            "title",
            "content_positive",
            "content_negative",
            "rating",
            ]
        labels = {
            'content_positive': _('Pros'),
            'content_negative': _('Cons'),
        }
        widgets = {
            "rating": forms.NumberInput(attrs={'class': 'hidden', 'id': 'rating'}),
        }