from django.db import models
from django.conf import settings
from business.models import Business
# Create your models here.

class Review(models.Model):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    RATINGS_CHOICES = (
        (ONE, '1'),
        (TWO, '2'),
        (THREE, '3'),
        (FOUR, '4'),
        (FIVE, '5'),
    )
    class Meta:
        verbose_name_plural = "reviews"
        ordering = ["-created"]
    title = models.CharField(max_length=120)
    content_positive = models.TextField()
    content_negative = models.TextField(blank=True, null=True)
    helpful_count = models.IntegerField(blank=True, null=True)
    rating = models.IntegerField(
        choices=RATINGS_CHOICES,
        default=ONE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="reviews")
    business = models.ForeignKey(Business, related_name="reviews")
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)