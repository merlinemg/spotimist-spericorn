from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404
from bookings.models import Booking
from .forms import ReviewForm
from spotimist import settings
from pytz import timezone
import datetime

# Create your views here.


def reviews_list(request):
    
    reviews = request.user.reviews.all()

    bookings_to_review = request.user.bookings.filter(reviewed=False, date_from__lt=datetime.datetime.now(timezone(str(settings.TIME_ZONE))))

    context = {
        "reviews": reviews,
        "bookings_to_review": bookings_to_review
    }

    return render(request, "reviews/index.html", context )


def create_review(request, pk):

    booking = get_object_or_404(Booking, pk=pk)

    # if booking doesn't belong to the logged in user and if review has been left already then raise 404
    if booking.reviewed or booking.user != request.user:
        raise Http404        

    if request.method=='POST':

        review_form = ReviewForm(request.POST or None)

        if review_form.is_valid():
            instance = review_form.save(commit=False)
            instance.user = request.user
            instance.business = booking.business
            booking.reviewed = True
            booking.save()
            instance.save()

            messages.success(request, 'Review added successfully!', extra_tags="review_added")
            return redirect ("reviews:list_my_reviews")
    else:
        review_form = ReviewForm()
    
    context = {
        "form": review_form,
        "business_review": "{} at {}".format(booking.get_booked_activity(), booking.business)
    }

    return render(request, "reviews/create.html", context )

    

def edit_review(request):
    return ""

def delete_review(request):
    return ""
