from django.contrib import admin
from .models import Review
# Register your models here.


class ReviewModelAdmin(admin.ModelAdmin):
    list_display = ["title", "rating", "created"]
    list_filter = ['business', 'rating']
    search_fields = ["title", "content_positive", "content_negative"]
    class Meta:
        model = Review


admin.site.register(Review, ReviewModelAdmin)