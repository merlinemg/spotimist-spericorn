from django.db import models
from django.utils.translation import gettext_lazy as _
from jsonfield import JSONField
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, YEARLY, MONTHLY, WEEKLY, DAILY, HOURLY
from datetime import datetime

# from activities.models import Activity

# Create your models here.

RECURRING = [
    (1, _("Every Day")),
    (2, _("Weekdays Only")),
    (3, _("Weekends Only")),
    (4, _("Once a Week")),
    (6, _("Monthly")),
    (7, _("Specificy Days")),
]


class ServiceType(models.Model):
    class Meta:
        verbose_name_plural = "service types"

    name = models.CharField(max_length=120)
    description = models.TextField(blank=True, null=True)
    requires_booking = models.BooleanField(
        _('requires booking'),
        default=False
    )
    is_subscription = models.BooleanField(
        _('is subscription'),
        default=False
    )
    is_active = models.BooleanField(
        _('is active'),
        default=False
    )
    name_code = models.CharField(max_length=120, blank=True, null=True)

    service_percentage = models.IntegerField()

    def __str__(self):
        return self.name


class Service(models.Model):
    class Meta:
        verbose_name_plural = "services"
        ordering = ["-created"]
        abstract = True

    name = models.CharField(max_length=120)
    # service_type = models.ForeignKey(ServiceType)
    short_description = models.TextField(blank=True, null=True)
    long_description = models.TextField(blank=True, null=True)
    activity = models.ForeignKey('activities.Activity')
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    price = models.DecimalField(
        max_digits=36,
        decimal_places=2,
    )
    service_type = models.ForeignKey(ServiceType)

    is_archived = models.BooleanField(_("is archived"), default=False)

    def is_editable_by(self, user):
        return self.activity.business.user == user

    def __str__(self):
        return self.name

    def logical_delete(self):
        self.is_archived = True


class RecurringService(Service):
    spaces = models.PositiveIntegerField(blank=True, null=True)
    start_date = models.DateField(verbose_name=_("start date"))
    start_time = models.TimeField(verbose_name=_("start time"))
    end_date = models.DateField(_("end date"), null=True, blank=True)
    end_time = models.TimeField(verbose_name=_("end time"), null=True, blank=True)
    repeat = models.IntegerField(
        _("repeat"), choices=RECURRING, default='1'
    )
    end_repeat = models.CharField(_("end repeat"), max_length=200)
    duration = models.PositiveIntegerField()

    def get_next_occurrence(self):
        import datetime

        now = datetime.datetime.now()
        dates = self.get_occurrences(self.start_date, None, self.end_repeat)

        for date in dates:
            if date > now:
                return date
                break

    def get_occurrences(self, original_start_date, start_date=None, end_repeat=None):
        day_repeat = end_repeat
        if self.repeat == 1:
            frequency = DAILY
        elif self.repeat == 4:
            frequency = WEEKLY
        elif self.repeat == 6:
            frequency = MONTHLY

        # Get start date parameter, if null use service start date
        if start_date == None:
            full_date = datetime.combine(original_start_date, self.start_time)
        else:
            full_date = datetime.combine(datetime.strptime(start_date, "%Y-%m-%d"), self.start_time)

        # Get end date parameter, if null add up 1 month to the start
        if end_repeat == None:
            end_repeat = full_date + relativedelta(months=1)
        else:
            array = []
            temp_array = []
            end_repeat = str(end_repeat)
            for end in end_repeat.split(','):
                if end not in temp_array:
                    array.append(end)
                    temp_array.append(end)
            try:
                sort_array = sorted(array, key=lambda x: datetime.strptime(x, '%d/%m/%Y'))
            except:
                sort_array = sorted(array, key=lambda x: datetime.strptime(x, '%Y-%m-%d'))
            for s_array in sort_array[-1:]:
                try:
                    s_date = datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                except:
                    s_date = datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                end_repeat = datetime.strptime(str(s_date), "%Y-%m-%d")
        end_day = day_repeat.split(',')
        date_list = []
        for i in end_day:
            try:
                fmt_date = datetime.strptime(i, '%d/%m/%Y')
            except:
                fmt_date = datetime.strptime(i, '%Y-%m-%d')
            date_list.append(fmt_date)
        if self.repeat == 2:
            dates = list(rrule(freq=DAILY, byweekday=(0, 1, 2, 3, 4), dtstart=full_date, until=end_repeat))
        elif self.repeat == 3:
            dates = list(rrule(freq=DAILY, byweekday=(5, 6), dtstart=full_date, until=end_repeat))
        elif self.repeat == 7:
            dates = date_list
        else:
            dates = list(rrule(freq=frequency, dtstart=full_date, until=end_repeat))
        return (dates)


class CourseService(Service):
    spaces = models.IntegerField()
    start_date = models.DateTimeField(verbose_name=_("start date"))
    end_date = models.DateTimeField(_("end date"))
    repeat = models.CharField(
        _("repeat"), max_length=15, choices=RECURRING, default='1'
    )
    specific_days = JSONField()
    end_repeat = models.DateField(_("end repeat"), null=True, blank=True)


class Occurrence(models.Model):
    event = models.ForeignKey(RecurringService, on_delete=models.CASCADE, verbose_name=_("Recurring Service "))
    title = models.CharField(_("title"), max_length=255, blank=True)
    description = models.TextField(_("description"), blank=True)
    start = models.DateTimeField(_("start"), db_index=True)
    end = models.DateTimeField(_("end"), db_index=True)
    cancelled = models.BooleanField(_("cancelled"), default=False)
    # original_start = models.DateTimeField(_("original start"))
    # original_end = models.DateTimeField(_("original end"))
    created_on = models.DateTimeField(_("created on"), auto_now_add=True)
    updated_on = models.DateTimeField(_("updated on"), auto_now=True)

    def __str__(self):
        return self.title
