# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-11 13:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0022_auto_20180606_0401'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicetype',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name='is active'),
        ),
        migrations.AddField(
            model_name='servicetype',
            name='name_code',
            field=models.CharField(blank=True, max_length=120, null=True),
        ),
    ]
