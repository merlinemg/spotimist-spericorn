# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-05 07:12
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0020_auto_20180605_0615'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='courseservice',
            name='client_price',
        ),
        migrations.RemoveField(
            model_name='recurringservice',
            name='client_price',
        ),
    ]
