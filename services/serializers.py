from .models import RecurringService
from rest_framework import routers, serializers, viewsets, generics

# Serializers define the API representation.
class RecurringServiceSerializer(serializers.HyperlinkedModelSerializer):
    occurrences_list = serializers.SerializerMethodField()
    class Meta:
        model = RecurringService
        fields = [
            'id', 
            'url', 
            'name',
            'price',
            'start_date',
            'start_time',
            # 'end_date',
            # 'end_time',
            'end_repeat',
            'spaces',
            'occurrences_list',
            'is_archived'
            ]

    def get_occurrences_list(self, obj):
        return obj.get_occurrences(obj.start_date, None, obj.end_repeat)
# ViewSets define the view behavior.
class RecurringServiceViewSet(viewsets.ModelViewSet):
    queryset = RecurringService.objects.all()
    serializer_class = RecurringServiceSerializer