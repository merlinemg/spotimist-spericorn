from django.contrib import admin
from .models import RecurringService, ServiceType

# Register your models here.


class ServiceModelAdmin(admin.ModelAdmin):
    list_display = ["name", "created"]
    list_filter = ["activity", "name"]
    search_fields = ["name", "description"]
    # list_oder = ["business"]
    class Meta:
        model = RecurringService

class ServiceTypeModelAdmin(admin.ModelAdmin):
    list_display = ["name" ]
    list_filter = ["name"]
    search_fields = ["name"]
    # list_oder = ["business"]
    class Meta:
        model = ServiceType

admin.site.register(RecurringService, ServiceModelAdmin)
admin.site.register(ServiceType, ServiceTypeModelAdmin)