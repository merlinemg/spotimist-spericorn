from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.http import Http404, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect, reverse
from activities.models import Activity
from .models import RecurringService, Occurrence, ServiceType
from .forms import ServiceForm, ServiceFormEdit
from django.db import connection
from django.core import serializers
from django.forms.models import model_to_dict
import json


# Create your views here.

def services_list(request, slug):
    activity = get_object_or_404(Activity, slug=slug)

    if not activity.is_editable_by(request.user):
        raise Http404("You can't access this object.")

    services = RecurringService.objects.filter(activity=activity, is_archived=False)
    type_service = ServiceType.objects.filter(is_active=True)

    context = {
        "activity": activity,
        "services": services,
        "type_service": type_service,
    }
    return render(request, "services/index.html", context)


@login_required
def services_create(request, slug):
    activity = get_object_or_404(Activity, slug=slug)
    form = ServiceForm(request.POST or None)
    
    if request.POST.get('service_type'):
        service_id = ServiceType.objects.get(name_code=request.POST.get('service_type'))
    else:
        service_id = ServiceType.objects.get(name_code='recurring')

    if form.is_valid():
        instance = form.save(commit=False)
        instance.activity = activity
        instance.service_type = service_id
        instance.save()
        form.save_m2m()
        return redirect(activity.get_services_url())
    # else:

    # messages.error(request, "Not Successfully Created")
    context = {
        "form": form,
    }
    return render(request, "services/create.html", context)


@login_required
def services_edit(request, slug, id):
    service = get_object_or_404(RecurringService, id=id)
    # service.get_occurrences(service.start_date, service.end_date)
    # print(connection.queries)
    # print("Is editable by " + request.user.first_name + " " + str(service.is_editable_by(request.user)))

    if not service.is_editable_by(request.user):
        raise Http404("You can't access this object.")

    form = ServiceFormEdit(request.POST or None, instance=service)
    if form.is_valid():
        instance = form.save()

        return redirect(reverse("services-list", kwargs={"slug": slug}))
    # else:
    #     messages.error(request, "Not Successfully Created")
    context = {
        "form": form,
    }
    return render(request, "services/edit.html", context)


@login_required
def services_delete(request, slug, id):
    service = get_object_or_404(RecurringService, id=id)
    service.logical_delete()
    service.save()
    # messages.success(request, "Successfully Deleted")

    return redirect(reverse("services-list", kwargs={"slug": slug}))


def occurences_json(request):
    # print ("query string " + str(request.GET.get("start")))
    # services = RecurringService.objects.all()  # or simply .values() to get all fields
    services = RecurringService.objects.filter(activity__business__user=request.user)
    # activities_list = list(activities)  # important: convert the QuerySet to a list object

    # RecurringService.objects.all()
    dates_array = ""

    start_query = request.GET.get("start")
    end_query = request.GET.get("end")

    occurrences = []

    for service in services:
        # dates = service.get_occurrences(service.start_date, start_query, end_query)
        dates = service.get_occurrences(service.start_date, None, service.end_repeat)
        for date in dates:
            occ = Occurrence()
            occ.title = service.name
            occ.start = date

            data = {'title': service.activity.name + " " + service.name, 'start': date}
            occurrences.append(data)

    # json_data = serializers.serialize('json', occurrences)

    # return render(request, "services/edit.html" )
    return JsonResponse(occurrences, safe=False)
