#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from django import forms
from .models import RecurringService


class ServiceForm(forms.ModelForm):
    class Meta:
        model = RecurringService
        fields = ["name", "price", "start_date", "start_time", "duration", "repeat", "end_repeat",
                  "spaces", ]
        widgets = {
            "start_date": forms.DateInput(
                attrs={'class': 'booking-date', 'placeholder': 'dd/mm/yyyy', 'readonly': 'true'}),
            # "end_date": forms.DateInput(
            #     attrs={'class': 'booking-date', 'placeholder': 'dd/mm/yyyy', 'readonly': 'true'}),
            "end_repeat": forms.DateInput(
                attrs={'class': 'booking-date', 'placeholder': 'dd/mm/yyyy', 'readonly': 'true'}),
            "repeat": forms.Select(attrs={'class': 'chosen-select-no-single'})
        }

    def clean_end_repeat(self):
        start_date = self.cleaned_data.get('start_date')
        end_repeat = self.cleaned_data.get('end_repeat')
        array = []
        temp_array = []
        for i in end_repeat.split(','):
            now = datetime.datetime.strptime(i, '%d/%m/%Y').date()
            if start_date and now:
                if now >= start_date:
                    if now not in temp_array:
                        array.append(str(now))
                        temp_array.append(str(now))
        try:
            sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
        except:
            sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
        for s_array in sort_array[-1:]:
            try:
                s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                s_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
            except:
                s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                s_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
            if start_date and s_date:
                if s_date < start_date:
                    raise forms.ValidationError("Date should be greater than or equal to end date...!")
        if sort_array == []:
            raise forms.ValidationError("Date should be greater than or equal to end date...!")

        return end_repeat

    def clean_price(self):
        price = self.cleaned_data.get('price')
        if price > 0 and price < 1:
            raise forms.ValidationError("Price should be minimum of €1")

        return price

    def clean_duration(self):
        duration = self.cleaned_data.get('duration')
        if duration < 1:
            raise forms.ValidationError("Duration should be minimum of 1hr")
        return duration


class ServiceFormEdit(forms.ModelForm):
    class Meta:
        model = RecurringService
        fields = ["name", "price", "start_date", "duration", "start_time", "repeat", "end_repeat",
                  "spaces", ]
        widgets = {
            "start_date": forms.DateInput(attrs={'class': 'booking-date', 'readonly': 'true', 'data-attr': ''}),
            # "end_date": forms.DateInput(attrs={'class': 'booking-date', 'readonly': 'true', 'data-attr': ''}),
            "repeat": forms.Select(attrs={'class': 'chosen-select-no-single', 'data-default-value': '2'}),
            "end_repeat": forms.DateInput(attrs={'class': 'booking-date', 'readonly': 'true', 'data-attr': ''}),
        }

    def clean_end_repeat(self):
        start_date = self.cleaned_data.get('start_date')
        end_repeat = self.cleaned_data.get('end_repeat')
        array = []
        temp_array = []
        for i in end_repeat.split(','):
            try:
                now = datetime.datetime.strptime(i, '%d/%m/%Y').date()
            except:
                now = datetime.datetime.strptime(i, '%Y-%m-%d').date()
            if start_date and now:
                if now >= start_date:
                    if now not in temp_array:
                        array.append(str(now))
                        temp_array.append(str(now))

        try:
            sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%d/%m/%Y'))
        except:
            sort_array = sorted(array, key=lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
        for s_array in sort_array[-1:]:
            try:
                s_date = datetime.datetime.strptime(s_array, '%d/%m/%Y').strftime('%Y-%m-%d')
                s_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()
            except:
                s_date = datetime.datetime.strptime(s_array, '%Y-%m-%d').strftime('%Y-%m-%d')
                s_date = datetime.datetime.strptime(s_date, '%Y-%m-%d').date()

            if start_date and s_date:
                if s_date < start_date:
                    raise forms.ValidationError("Date should be greater than or equal to start date")
        if sort_array == []:
            raise forms.ValidationError("Date should be greater than or equal to start date")

        return end_repeat

    def clean_price(self):
        price = self.cleaned_data.get('price')
        if price > 0 and price < 1:
            raise forms.ValidationError("Price should be minimum of €1")
        return price

    def clean_duration(self):
        duration = self.cleaned_data.get('duration')
        if duration < 1:
            raise forms.ValidationError("Duration should be minimum of 1hr")
        return duration
